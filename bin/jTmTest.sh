#!/bin/sh

# Check the java bin
declare java="$TDAQ_JAVA_HOME"/bin/java
if [ -z "$TDAQ_JAVA_HOME" ]; then
     echo "Could not find java binary. Please, check the TDAQ_JAVA_HOME value."
     exit
fi

# Tune jacorb, increase the size of the threadpool with the properties:
declare jacorbProperties="-Djacorb.connection.client.idle_timeout=1000 \
                          -Djacorb.connection.client.max_receptor_threads=4000 \
                          -Djacorb.connection.server.max_receptor_threads=4000"

#declare jvmOpt="-Xms128M -Xmx2048M";

declare classpath="${TDAQ_CLASSPATH}:${CLASSPATH}";


#Disable the ERS signal handler
export TDAQ_ERS_NO_SIGNAL_HANDLERS=1

# Start 
exec $java $jacorbProperties -Dtdaq.partition=${TDAQ_PARTITION} -Dtdaq.dbconfig=${TDAQ_DB} -cp $classpath jTestManager.TmTestingClass "$@"

