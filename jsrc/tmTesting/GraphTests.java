package tmTesting;

import org.jgrapht.*;
import org.jgrapht.alg.CycleDetector;
import org.jgrapht.graph.*;
import org.jgrapht.traverse.TopologicalOrderIterator;

public class GraphTests {

	private GraphTests() {
	} // ensure non-instantiability.

	/**
	 * The starting point for the demo.
	 * 
	 * @param args ignored.
	 */
	public static void main(String[] args) {

		// create a graph based on URL objects
		DirectedGraph<String, DefaultEdge> graph = createGraph();
		
		CycleDetector<String, DefaultEdge> cycleDetector = new CycleDetector<>(graph);
		
		System.out.println("Checking for cycles... " );
		if( cycleDetector.detectCycles() ) {
			System.out.println("detected at least one cycle!");
		}
		else {
			System.out.println(" no cycles found!");
		}

		// note directed edges are printed as: (<v1>,<v2>)
		System.out.println("\ntoString: " + graph.toString() + "\n");
		
		TopologicalOrderIterator<String, DefaultEdge> orderIterator = new TopologicalOrderIterator<String, DefaultEdge>(graph);
		while (orderIterator.hasNext()) {
            System.out.print(orderIterator.next() + ", ");
         }
		System.out.println("");
		
	}

	/**
	 * Creates a toy directed graph based on URL objects that represents link
	 * structure.
	 * 
	 * @return a graph based on URL objects.
	 */
	private static DirectedGraph<String, DefaultEdge> createGraph() {
		DefaultDirectedGraph<String, DefaultEdge> g = new DefaultDirectedGraph<String, DefaultEdge>(DefaultEdge.class);
		
		
		
		String v2 = "2";
		String v3 = "3";
		String v5 = "5";
		String v7 = "7";
		String v8 = "8";
		String v9 = "9";
		String v10 = "10";
		String v11 = "11";

		// add the vertices
		g.addVertex(v2);
		g.addVertex(v3);
		g.addVertex(v5);
		g.addVertex(v7);
		g.addVertex(v8);
		g.addVertex(v9);
		g.addVertex(v10);
		g.addVertex(v11);
		
		
		

		// add edges to create linking structure
		g.addEdge(v7, v11);
		g.addEdge(v7, v8);
		g.addEdge(v5, v11);
		g.addEdge(v3, v8);
		g.addEdge(v3, v10);
		g.addEdge(v11, v2);
		g.addEdge(v11, v9);
		g.addEdge(v11, v10);
		g.addEdge(v8, v9);


		return g;
	}


}
