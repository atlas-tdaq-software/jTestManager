package jTestManager;


/**
 * Thrown when TestManager has no hosts to be used as testHosts.
 */
public class NoTestHostExcepion extends TestManagerException {


	private static final long serialVersionUID = 5250757719729145805L;

	public NoTestHostExcepion(String string, Exception ex) {
		super(string, ex);
	}

	public NoTestHostExcepion(String string) {
		super(string);
	}

}
