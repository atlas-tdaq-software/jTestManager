package jTestManager;

/**
 * Thrown when a wrong configuration in the databases is detected. 
 * 
 * e.g. A cycle is found while trying to solve dependencies in a set of Tests or Executable 
 *
 */
public final class WrongConfigurationException extends TestManagerException {

	private static final long serialVersionUID = -3013199436761978488L;

	public WrongConfigurationException(String string) {
		super(string);
	}
	
	public WrongConfigurationException(Exception ex) {
		super(ex);
	}	

}
