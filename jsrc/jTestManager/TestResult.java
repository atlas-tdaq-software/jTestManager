package jTestManager;

/**
 * 
 *  As defined in <b>daq::tmgr </b> 
 *  https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/doxygen/nightly/html/da/d98/namespacedaq_1_1tmgr.html
 *  
 *  Generally, be consistent with the twiki:
 *  	https://twiki.cern.ch/twiki/bin/viewauth/Atlas/DaqHltTestManager
 *
 */
public enum TestResult {

	TM_PASS (0),			// test completed and confirmed the tested functionality 
	TM_FAIL (183),			// test completed and confirmed that component is not working properly 
	
	
	TM_UNRESOLVED (184),	// the test could not make an assessment about the component it was supposed to test
											//test completed but it could not verify the functionality due to internal issues 
											// (internal test problems, lack of resources, system errors, timeout etc.)
	TM_UNTESTED (185),		// test was not executed, e.g. some problems with test manager itself or with test configuration 
	
	
	TM_UNSUPPORTED (186),	// test was not executed (e.g. component is untestable) - in practice TmUntested is returned in this case 
	TM_UNDEF (182);			// initial value, before test is started

	
	private final int result;
	
	
	TestResult( int res ) {
		this.result = res;
	}
	
	public int getInt() {
		return this.result;
	}
}
