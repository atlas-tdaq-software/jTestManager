package jTestManager;

import ipc.InvalidObjectException;
import ipc.InvalidPartitionException;
import ipc.TestFailed;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import config.SystemException;
import pmgClient.CorbaException;
import pmgClient.Handle;
import pmgClient.NoSuchPmgServerException;
import pmgClient.NonExistingProcessFile;
import pmgClient.PmgClientException;
import pmgClient.Process;
import pmgClient.ProcessDescription;
import pmgClient.ServerException;
import pmgpub.ProcessState;
import TM.Executable;
import TM.ExecutableTest;
import TM.ExecutableTest_Helper;
import TM.Test;
import TM.Test4CORBAServant;
import TM.Test4CORBAServant_Helper;
import dal.Computer;
import dal.ComputerProgram;
import dal.Computer_Helper;
import dal.Partition;
import dal.Tag;
import ers.Issue;

/**
 * 
 * This is the Runnable that is actually using the PmgClient to launch the Executables for a specific Test
 * 		and create more SpecificTestRunnable threads for the other Tests of the component.
 * 
 * Upon receiving a PmgClient callback a new Thread is created and -if applicable- it takes care of launching the next set of independent Tests
 *
 */
class SpecificTestRunnable implements Runnable {
	
	final private Test test;
	final private Tester tester;
	final private Partition partition;
	final private String partitionName;
	
	// used to manage pmg callbacks that were received out of order and procs that seemed not to have started
	final private ConcurrentMap<String, Boolean > processesMap;
	
	final private List<String> failOrSyncError;
	final private boolean isSingleTest;
	final private boolean localDiscardOutput;
	
	private Cache.CachedSpecificTestResult cachedResult;
	private boolean repeatIfUnresolved;
	private CountDownLatch execsToReachRunningState;
	private SpecificTestResult specificTestResult;
	private AtomicInteger procsStarted;
	private AtomicBoolean launchedAll;

	
	
	public static AtomicInteger pmgNON_RUN_CbsReceived = new AtomicInteger(0);				// DBG rm
	public static AtomicInteger pmgRunningReceived = new AtomicInteger(0);
	public static AtomicInteger pmgRunningAssumed = new AtomicInteger(0);
	public static AtomicInteger pmgProcessesStarted = new AtomicInteger(0);
	public static AtomicInteger exceptionsCaught = new AtomicInteger(0);
	public static AtomicInteger pmgALLCBS = new AtomicInteger(0);
	public static AtomicInteger pmgCbIsFailOrSyncerror = new AtomicInteger(0);
	public static AtomicInteger pmgCbButProcNotInTheMap = new AtomicInteger(0);

	public static AtomicInteger testFinishedNormal = new AtomicInteger(0);
	public static AtomicInteger testFinishedZeroLaunched = new AtomicInteger(0);
	public static AtomicInteger testFinishedCORBA = new AtomicInteger(0);
	public static AtomicInteger cachedTests = new AtomicInteger(0);
	private int increasingCounter = 0;
	
	
	/**
	 * The callback from the pmg client.
	 * Inside here is the logic regarding which process is that (is it the last on for this Test?) 
	 * along with the relevant countings
	 * 
	 *
	 */
	class CallbackFromPMG implements pmgClient.Callback {
		

		@Override
		public void callbackFunction(final pmgClient.Process linkedProcess) {
			
			try {
				tester.getTm().getReadLock().lock();
				/*
				 * If the exception was because of a CORBA timeout (client side) the there is a change that the process
				 * will be launched by the server. This is likely to happen if the exception in the pmgClient was thrown by
				 * the really_start() call to the server. If that is the case, maybe we receive a callback for a process that
				 * we thought was not launched.
				 * 
				 *  So, if the current callback's process is not in the map with the processes we launched, then it must be the case mentioned above
				 */
				
				pmgALLCBS.incrementAndGet();
				
	//			System.out.println("  ++ PMG cb. State: " + linkedProcess.getProcessStatusInfo().state.toString() 
	//						+ " \treturn value: " + linkedProcess.getProcessStatusInfoq().info.exit_value
	//						+ " \tfrom: " + linkedProcess.getHandle() + "   for Test:\t" + test.UID());
	//			
				
				final Handle handle = linkedProcess.getHandle();
	
				final String mapKey = tester.getBaseclassName() + "@" + tester.getCompID() + "@" + test.UID() + "@" + handle.getAppName();
				/*
				 * This check is for pmg callbacks out of order and cases were process start threw exception but the process was launched
				 */
				if( ! processesMap.containsKey(mapKey) ) {
					pmgCbButProcNotInTheMap.incrementAndGet();
					linkedProcess.unlink(); // DISCUSS
					return;
				}
				
				/*
				 * Get the info that we will use, while we are still inside the callback not in the new thread (if one is created)
				 * When the callback exits, linkedProcess object could be updated with new information
				 * and hence is should not be used after this callback exits 
				 */
				final int state = linkedProcess.getProcessStatusInfo().state.value();
				
				if (state == ProcessState.RUNNING.value()) {
					processesMap.replace(mapKey, Boolean.valueOf(true));	// received RUNNING
					execsToReachRunningState.countDown();
					pmgRunningReceived.incrementAndGet();
					return;
				} 
				SpecificTestRunnable.pmgNON_RUN_CbsReceived.incrementAndGet();
				

				final int exitValue;
				
				//DBG
				if( state == ProcessState.FAILED.value() || state == ProcessState.SYNCERROR.value()) {
//					System.err.println("!!!!!!!!!!!!!!!!    state == ProcessState.FAILED.value() || state == ProcessState.SYNCERROR.value()     !!!!!!!!!!");
					pmgCbIsFailOrSyncerror.incrementAndGet();
					
					execsToReachRunningState.countDown();	// these processes will never reach running state
					
					processesMap.remove(mapKey);
					tester.getTmHandle().removeProcess(mapKey);
					
					final String appName = linkedProcess.getHandle().getAppName();   // appName is: exec.UID() + "-" + increasingCounter++;
					failOrSyncError.add( appName.substring(0, appName.lastIndexOf("-")) );
					exitValue = TestResult.TM_FAIL.getInt();
							// because in these cases linkedProcess.getProcessStatusInfo().info.exit_value = 0  and should be ignored
				} else {
					exitValue = linkedProcess.getProcessStatusInfo().info.exit_value;
				}
				
				
				
				/*
				 *	Return from the callback ASAP and run the rest in some other thread from the thread pool
				 */
				try {
					tester.getTm().getThreadPool().execute(new Runnable() {
						public void run() {
					
							if(localDiscardOutput == false) {
								// update the streams
								try {
									specificTestResult.updateStdout(linkedProcess.outFileStr());
								} catch (NoSuchPmgServerException e) {
									final String msg = "NoSuchPmgServerException while trying to get the out stream of the process " + linkedProcess.getHandle();
									ers.Logger.error(new ers.Issue(msg, e));
									specificTestResult.updateStderr(msg);
								} catch (NonExistingProcessFile e) {
	//								ers.Logger.log(e);
									specificTestResult.updateStderr(e.getMessage());
								} catch (CorbaException e) {
									ers.Logger.error(e);
									specificTestResult.updateStderr(e.getMessage());
								}
								
								try {
									specificTestResult.updateStderr(linkedProcess.errFileStr());
								} catch (NoSuchPmgServerException e) {
									final String msg = "NoSuchPmgServerException while trying to get the out stream of the process " + linkedProcess.getHandle();
									ers.Logger.error(new ers.Issue(msg, e));
									specificTestResult.updateStderr(msg);
								} catch (NonExistingProcessFile e) {
	//								ers.Logger.log(e);					// DISCUSS pmgServer should separate exceptions
									specificTestResult.updateStderr(e.getMessage());
								} catch (CorbaException e) {
									ers.Logger.error(e);
									specificTestResult.updateStderr(e.getMessage());
								}	
							}
					
//							System.out.println("  ++ PMG cb. State: " + linkedProcess.getProcessStatusInfo().state.toString() 
//							+ " \treturn value: " + linkedProcess.getProcessStatusInfo().info.exit_value
//							+ " \tfrom: " + linkedProcess.getHandle() + "   for Test:\t" + test.UID());
							
							
		
							if ( processesMap.remove(mapKey) == false) {
								/* If false, it means that this is the first callback we get for the process and it is NOT the RUNNING.
								 * This is the case where callbacks arrived out of order and the RUNNING is expected after this one.
								 * Since the process won't be in the Map the RUNNING callback will return immediately,
								 * 		so we do the counting now.
								 */
		
								linkedProcess.unlink();
								
								// TODO check if failed or sync error (no running in that case)
								execsToReachRunningState.countDown();
								pmgRunningAssumed.incrementAndGet();
							}
							
							tester.getTmHandle().removeProcess(mapKey);
				
							if( state == ProcessState.SIGNALED.value() ) {	
								/*
								 * Process was signaled; this could suggest a Test timeout or that generally the process was signaled.
								 */
								specificTestResult.updateWithExecReturnCode(TestResult.TM_UNTESTED);
								specificTestResult.updateStdout("[Process was signaled, i.e. from a Test timeout or other reason]");
							} else {
								specificTestResult.updateWithExecReturnCode(exitValue);
							}
							
							try {
								tester.addFailures(test, exitValue);
							}
							catch (config.ConfigException ex) {
								ers.Logger.error( ex );
							}
							catch (InterruptedException e) {
								Thread.currentThread().interrupt();	// testFinished will handle it or will exit anyway
							}
							
		
							if ( procsStarted.decrementAndGet() == 0 && launchedAll.compareAndSet(true, false) ) {	// check if it was the last process for this specific Test (ExecutableTest)
								testFinishedNormal.incrementAndGet();
								testFinished(specificTestResult);
							} else if(procsStarted.get() < 0) {
								System.err.println(" **** 1 **** was < 0 but the smallest expected value is 0"); 
								//DBG rm me after dbg, [could this happen normally?]
							}
						}
					});
				} catch (RejectedExecutionException ex) {
					ers.Logger.error( new ers.Issue(ex));
				}
			} finally {
				tester.getTm().getReadLock().unlock();
			}	
			
		}	// endof callbackFunction method
		
	}	// endof class CallbackFromPMG
	
	
	
	
	/**
	 * 
	 * @param test
	 * @param tester
	 * @param isSingleTest signifies whether this runnable is from a Tester that had only one Test (this one)
	 */
	SpecificTestRunnable(final Test test, final Tester tester, final boolean isSingleTest) throws config.ConfigException {
		this.test = test;
		this.tester = tester;
		this.partition = tester.getTm().getPartition();
		this.partitionName = tester.getTm().getPartitionName();
		this.processesMap = new ConcurrentHashMap<>(4, 0.9f, 1);
		this.failOrSyncError = Collections.synchronizedList(new ArrayList<String>());
		this.specificTestResult = new SpecificTestResult(test);
		this.launchedAll = new AtomicBoolean(false);
//		this.stopThisTest = new AtomicBoolean(false);
		this.procsStarted = new AtomicInteger(0);
		this.repeatIfUnresolved = test.get_RepeatIfUnresolved();
		this.isSingleTest = isSingleTest;
		
		this.localDiscardOutput = tester.getTm().getDiscardOutput();
	}
	
	
	
	

	@Override
	public void run() {
//		System.out.println("thread for test: " + this.test.UID());

		if (Thread.currentThread().isInterrupted()) {
			ers.Logger.debug(1, ("Thread {" + Thread.currentThread().getName() +"} (SpecificTestRunnable.run) was interrupted"));
			unlinkAllAndRestoreInterrupted();
			return;
		}
		this.cachedResult = this.tester.getTm().initializeToCache(this.tester.getCompCacheUID(), this.specificTestResult);
		
		final ExecutableTest execTest = ExecutableTest_Helper.cast(this.test);
		
		if (execTest != null) { // Check if it is an ExecutableTest

			/*
			 *	Solve the dependencies among the Executables of the Test 
			 */
			DependenciesSolver executableDependSolver = null;
			try {
				executableDependSolver = new DependenciesSolver(execTest.get_Runs());
			} catch (config.ConfigException | WrongConfigurationException e) {
				ers.Logger.error(e);
				specificTestResult.updateWithExecReturnCode(TestResult.TM_UNTESTED);
				specificTestResult.updateStderr(e.getMessage());
				testFinished(specificTestResult);
				return;
			}
			final List<List<Executable>> execInOrder = executableDependSolver.getGroupedTopologicalOrderedExecutables();
			
			
			/*
			 * Launches the Executables of the Test, taking into account that each group (Set) requires that
			 * all the processes of the previous Set have reached RUNNING state.
			 * (This is a requirement by the the InitDependsOn relationship of the Executable)
			 */
			final int numOfIndependentGroups = execInOrder.size();
			int currentGroup = 0;
			int lunchedInTotal = 0;
			
			for (final List<Executable> independentExecs : execInOrder) {
				
				currentGroup++;
				execsToReachRunningState = new CountDownLatch(independentExecs.size());
				
				try {
					final int launchedInCycle = runExecutables(independentExecs);
					lunchedInTotal += launchedInCycle;
					//this.procsRunning.getAndAdd(launchedInCycle);
					for( int i=0; i < independentExecs.size() - launchedInCycle ; i++ ) {	// countDown the number of the processes not launched
						execsToReachRunningState.countDown();
					}
				
					/*
					 * Wait for all the executables in the set to reach RUNNING before moving the next set of executables.
					 * Dependencies between executables are not common, so most of the time there is no waiting at all
					 * 
					 */
					if (currentGroup < numOfIndependentGroups ) {		 // Wait only if there is a next group
						while(! execsToReachRunningState.await(4, TimeUnit.SECONDS) ) {
							System.out.println("currentGroup: " + currentGroup + " numOfIndependentGroups:" + numOfIndependentGroups
									+ "   waiting for execs to reach RUNNING state."
									+ " Test [" + this.test.UID() + "], component [" + this.tester.getCompID() + "]");
						}
					}
				} catch (config.ConfigException e) {
					ers.Logger.error(e);
					specificTestResult.updateWithExecReturnCode(TestResult.TM_UNTESTED);
					specificTestResult.updateStderr(e.getMessage());
					testFinished(specificTestResult);
					return;
				}
				catch (InterruptedException ex) {
					unlinkAllAndRestoreInterrupted();
					return;
				}						
			}	// endof for all sets of independent Executables
			
			this.launchedAll.set(true);		// 'launched' doesn't necessarily mean that the process will reach Running state, e.g. maybe they reach Fail
				
			/* 
			 * There might be a case when no Executables were launched. If so, then make sure to add the result for the test and finish.
			 */
			if ( lunchedInTotal == 0 ) {
				testFinishedZeroLaunched.incrementAndGet();
				testFinished(specificTestResult);
			}
			/*
			    condition in cb is:  procsRunning.decrementAndGet() == 0 && launchedAll.compareAndSet(true, false)   // DBG rm linesss
			 */
			if (  procsStarted.get() == 0 && launchedAll.compareAndSet(true, false) ) {
				testFinishedZeroLaunched.incrementAndGet();
				testFinished(specificTestResult);
			}
			
			return;
		}	// end of if (execTest != null)

		
		
		// was not ExecutableTest. It has to be a Test4CORBAServant
		Test4CORBAServant corbaTest = Test4CORBAServant_Helper.cast(test);
		
		if (corbaTest != null) { // Check if it is a Test4CORBAServant
			String servantName, servantType, compID ;
			ipc.Partition ipcPartition ;

			try {
			final String cPartitionName = corbaTest.get_CPartitionName();
			ipcPartition = new ipc.Partition(cPartitionName);
			
			compID = this.tester.getCompID();
			servantName = this.tester.substituteParam(corbaTest.get_ServantName());
			servantType = this.tester.substituteParam(corbaTest.get_ServantType());
			} catch (config.ConfigException | WrongConfigurationException e1) {
				specificTestResult.updateWithExecReturnCode(TestResult.TM_UNTESTED);
				specificTestResult.updateStderr(e1.getMessage());
				testFinished(specificTestResult);
				return;
			} catch (InterruptedException e) {
				unlinkAllAndRestoreInterrupted();
				return;
			}
			
			
			if (servantName.isEmpty()) {
				servantName = compID;
			}
			
			if (servantType.isEmpty()) {
				specificTestResult.updateWithExecReturnCode(TestResult.TM_UNTESTED);
				specificTestResult.updateStderr("IPC InterfaceName not defined for component: " + servantName);
				testFinished(specificTestResult);
				return;
			}
			
			ipc.servant servant = null;
			try {
				if(Thread.currentThread().isInterrupted()) {
					unlinkAllAndRestoreInterrupted();
					return;
				}
				servant = ipcPartition.lookup(servantType, servantName);	// throws InvalidPartitionException and InvalidObjectException
				servant.test(this.test.UID());
				specificTestResult.updateWithExecReturnCode(TestResult.TM_PASS);
			} catch (TestFailed e) {
				if (e.error == TestResult.TM_UNRESOLVED.getInt() && this.repeatIfUnresolved) {	// try once more
					try {
						servant.test(this.test.UID());
						specificTestResult.updateWithExecReturnCode(TestResult.TM_PASS);
					} catch (TestFailed e1) {
						specificTestResult.updateWithExecReturnCode(e.error);
						specificTestResult.updateStderr(e.what);
					} catch (org.omg.CORBA.SystemException e1) {
						specificTestResult.updateStderr(e1.getMessage());
						specificTestResult.updateWithExecReturnCode(TestResult.TM_FAIL);
					}
				}else {
					specificTestResult.updateWithExecReturnCode(e.error);
					specificTestResult.updateStderr(e.what);
				}
			} catch (InvalidPartitionException e) {		// DISCUSS this and the following exceptions may be differentiated
				specificTestResult.updateWithExecReturnCode(TestResult.TM_FAIL);
				specificTestResult.updateStderr(e.message());
			} catch (InvalidObjectException e) {
				specificTestResult.updateWithExecReturnCode(TestResult.TM_FAIL);
				specificTestResult.updateStderr(e.message());
			} catch (org.omg.CORBA.SystemException e) {
				specificTestResult.updateStderr(e.getMessage());
				specificTestResult.updateWithExecReturnCode(TestResult.TM_FAIL);
			} finally {
										// This was a Test4CORBAServant, so we are done with this Test.
				testFinishedCORBA.incrementAndGet();
				testFinished(specificTestResult);
			}
			
		} else {
			ers.Logger.error(new Issue("Test was neither an ExecutableTest nor a Test4CRBAServant (!?)"));
			specificTestResult.updateWithExecReturnCode(TestResult.TM_UNTESTED);
			specificTestResult.updateStderr("Test was neither an ExecutableTest nor a Test4CRBAServant (!?)");
			testFinished(specificTestResult);
		}
	}
	
	
	
	/**
	 * 
	 * @return the Test that this Runnable is executing
	 */
	Test getTest() {
		return this.test;
	}
	
	
	
	
	/**
	 * Launches the given executables programs all at once (they have to be independent)
	 * 
	 * @param independentExecSet
	 * @throws InterruptedException 
	 */
	private int runExecutables(List<Executable> independentExecSet) throws config.ConfigException, InterruptedException {
		
		int totalExecsLaunched = 0;
		
		for (final Executable exec : independentExecSet) {
			
			boolean willRun = true;
			String hostStr = null;
			List<String> argsList = null;

			try {

			// check its dependencies. If at least one did not reach running state 
			for (final Executable initDependsOn : exec.get_InitDependsOn() ) {
				if ( this.failOrSyncError.contains(initDependsOn.UID()) ) {		// if that dependency did not reach Running state
					this.failOrSyncError.add(exec.UID());	// will also not launch the current one
					willRun = false;
					break;
				}
			}
			if(willRun == false) {
				continue;	// move to the next
			}

				if (exec.get_Host().equals("")) {
					hostStr = this.tester.getTm().getTestHost().UID();
				} else {
					hostStr = this.tester.substituteParam(exec.get_Host());		//  throws WrongConfigurationException
				}
				
				final String defaultParams = exec.get_Executes().get_DefaultParameters();
				String[] args = null;
				if (defaultParams.isEmpty()) {
					args = this.tester.substituteParam( exec.get_Parameters() ).split(" ");	// throws WrongConfigurationException
				} else {
					args = this.tester.substituteParam( defaultParams + " " + exec.get_Parameters() ).split(" ");// throws WrongConfigurationException
				}
				
				argsList = Arrays.asList(args);
				if( this.tester.getTm().getIsVerbose() ) {
					argsList.add("-v");
				}
			} catch (config.ConfigException | WrongConfigurationException e) {
				ers.Logger.error(e);
				specificTestResult.updateWithExecReturnCode(TestResult.TM_UNTESTED);
				specificTestResult.updateStderr("There was some thing wrong in the configuration of the executable \'"
						+ exec.UID() + "\' msg: " + e.getMessage());
				continue;					// XXX
			}
			

			final Map<String, String> env = new HashMap<>();
			final List<String> execLocationsList = new ArrayList<>();
			
			Computer host = Computer_Helper.get(this.tester.getTm().getConfig(), hostStr);
			boolean wasFound = false;
			
			if (Thread.currentThread().isInterrupted()) {
				throw new InterruptedException("Thread {" + Thread.currentThread().getName() 
						+ "} (SpecificTestRunnable.runExecutables before checking tags compatibility) was interrupted");
			}
			
			for (Tag tag : this.partition.get_DefaultTags()) {
				if (dal.Algorithms.is_compatible(tag, host, this.partition)) {
					try {
						exec.get_Executes().get_info(env, execLocationsList, this.partition, tag, host );	// fills env & execLocationsList
						wasFound = true;
						break;		// when a match is found, stop
					} catch (SystemException ex) {	// thrown by get_info
						ers.Logger.warning(ex);
						continue;
					}
				}
			}
			if( wasFound == false ) {			
				specificTestResult.updateWithExecReturnCode(TestResult.TM_UNTESTED);
				specificTestResult.updateStderr("None of the partition's \'" + this.partitionName
						+ "\' default tags is compatible with the host \'" + hostStr + "\'");
				continue;
			}
			
			StringBuilder execLocations = new StringBuilder(256);
			for( final String p : execLocationsList) {
				execLocations.append(p);
				execLocations.append(":");
			}
			if (execLocations.length() != 0) {
				execLocations.deleteCharAt(execLocations.length()-1);	// remove the last ':'
			}
			
			ComputerProgram pcProg = exec.get_Executes();
			String rmSwObj = "";
			if (pcProg.get_Needs() != null && pcProg.get_Needs().length != 0 ) {
				rmSwObj = pcProg.UID();
			}
			
			final String appName = exec.UID() + "-" + increasingCounter++;

			StringBuilder dbgStr = new StringBuilder();

			dbgStr.append("Creating process desription for app: ");
			dbgStr.append(appName);
			dbgStr.append(" in host: ");
			dbgStr.append(hostStr);
			dbgStr.append(" with arguments: ");

			for( String arg : argsList ) {
				dbgStr.append(arg + " ");
			}
			dbgStr.append("\n");

			ers.Logger.debug(0, dbgStr.toString() );

			final ProcessDescription processDescr = new ProcessDescription.ProcessDescriptionBuilder(
						this.tester.getTm().getPmg(),
						hostStr,
						this.partitionName,
						appName,
						execLocations.toString(),
						env)
					.startArgs(argsList)
					.initTimeout(exec.get_InitTimeout())
					.autoKillTimeout(test.get_Timeout())
					.rmSwobject(rmSwObj)
					.nullLogs(this.tester.getTm().getDiscardOutput())
					.wd(this.partition.get_WorkingDirectory())
//					.wd(this.tester.getTm().randomGen.nextInt(1000) % 3 == 0 ? "/" : this.partition.get_WorkingDirectory())		//DBG
					.logPath(this.partition.get_LogRoot() + "/" + this.partition.UID())
					.build();

			final CallbackFromPMG myCallback = new CallbackFromPMG();
			final String mapKey = this.tester.getBaseclassName() + "@" + this.tester.getCompID() + "@" + this.test.UID() + "@" + appName;
			
			
			if (Thread.currentThread().isInterrupted()) {
				throw new InterruptedException("Thread {" + Thread.currentThread().getName() 
						+ "} (SpecificTestRunnable.runExecutables before process start) was interrupted");
			}
			
			/*
			 * Add it in the map beforehand. Remove it, if it failed.
			 * Same for counting processes running (procsRunning)
			 * Notice: a timeout Exception does not necessarily mean that the pmg server will not launch it! 
			 * Check the comment at the beginning of the CallbackFromPMG.callbackFunction()
			 */
			if( this.processesMap.put(mapKey, Boolean.valueOf(false)) != null ) {	// 'false' in map means "no RUNNING callback received yet"
				// the map had previous value with the same key
				ers.Logger.fatal(new ers.Issue("the map had previous value with the same key " + mapKey + "\n\n\n\n\n"));
			}
			
			this.procsStarted.incrementAndGet();	// increase in advance and rm in case of exception
			
			try {
				pmgClient.Process p = processDescr.start(myCallback);
				this.tester.getTmHandle().addProcess(mapKey, p);
				
				SpecificTestRunnable.pmgProcessesStarted.incrementAndGet();
				totalExecsLaunched++;
			} catch (PmgClientException e) {
				this.tester.getTmHandle().removeProcess(mapKey);// this is probably not needed here
				
				if(	this.processesMap.remove(mapKey) == null ) {
//					System.err.println("---- tried to remove but was not there (cb received in the meanwhile");
				}
				this.procsStarted.decrementAndGet();
				SpecificTestRunnable.exceptionsCaught.incrementAndGet();
//				ers.Logger.error(new ers.Issue("Exception during start() of the process with application name: "	// DISCUSS
//						+ processDescr.getAppName() + " on host: " + processDescr.getHost()
//						+ " for Test: " + this.test.UID() + " of Testable: " + this.tester.getCompID(), e));
				specificTestResult.updateWithExecReturnCode(TestResult.TM_UNTESTED);
				specificTestResult.updateStderr("Exception during start() of the process: " + e.getMessage());			
			}

		}  // endof for(every executable in the set)
		
		return totalExecsLaunched;
	}
	
	
	
	
	
	
	/**
	 *
	 *	Notes: This method is called always in places were methods exit/return after its call.
	 * 		So in case the thread was interrupted, it just performs the cleanup (unlinks all processes, etc),
	 * 		 returns and the methods that called it return right after it.
	 * 	It is suggested that the above "contract" is preserved 
	 * @param specificTestResult
	 */
	private boolean testFinished(SpecificTestResult specificTestResult ) {
		
		if (Thread.currentThread().isInterrupted()) {
			ers.Logger.debug(1, "Thread {" + Thread.currentThread().getName() + "} (SpecificTestRunnable.testFinished) was interrupted");
			unlinkAllAndRestoreInterrupted();
			return false;
		}
		
		if ( repeatIfUnresolved && specificTestResult.getTestReturnCode() == TestResult.TM_UNRESOLVED.getInt() ) {			
			try {
				final SpecificTestRunnable runnable = new SpecificTestRunnable(this.test, this.tester, this.isSingleTest );
				runnable.setRepeatIfUnresolved(false);		// To avoid repeating more than once
				specificTestResult.updateStdout("[Tm message: Test was executed before and finished with UNRESOLVED. Repeating the Test once more.]");
				this.tester.getTm().getThreadPool().execute(runnable);
			} catch (config.ConfigException | RejectedExecutionException ex) {
				ers.Logger.error( new ers.Issue(ex));
				return false;
			}
			ers.Logger.warning(new ers.Issue("Test " + this.test.UID() + " finished with result UNRESOLVED and repeatIfUnresolved was set. "
					+ "Result is ingored and the Test will be executed once more. (1st run output was: " + specificTestResult.getStdout() + ")"));
			return true;     // careful not to change the Tester's counters if we repeat
		}
		

		this.tester.getCompResult().addResult(specificTestResult);			// addResult also updates the status of the whole component
		if (this.cachedResult != null) {
			this.cachedResult.commit();		// commit to cache, i.e. the Test is no longer running
		}
		
		if ( this.tester.getCurrentlyIndependentTestsToFinish().decrementAndGet() == 0) {	// check whether we finished with the current group
			/*
			 * Only one thread can enter here  
			 */
			
			
			//System.out.println("   Test: " + this.runnable.getTest().UID() + " : was the last in the set of independent tests!");
			
			// did all the test in the set, start with those in the next set (if there is one)!
			
			int groupsRemaining = this.tester.getGroupsToFinishNum().decrementAndGet();
			if ( groupsRemaining == 0 ) {				
											// we are done with the tests for this component. callback
				
	//			System.out.println("   Test: " + this.runnable.getTest().UID() + " : was the last one in the last group!"
	//					+ "Call the callback for : "
	//					+ (this.tester.isApp() ? this.tester.getAppConf().get_app_id() : this.tester.getTestableObject().UID()) );
				this.tester.callUsersCallback();
//				Tester.tm_cbs++;
//				System.out.println("tmcb called: " + Tester.tm_cbs);
				
			} else {
				final int nextGroupIndex = this.tester.getAllTestsGroupedInOrder().size() - this.tester.getGroupsToFinishNum().get();
				/*	 nextGroupIndex ranges : [ 1, totalNumOfGroups-1 ] .
				 Remember that (at least) the first group (group 0) was started by the Tester.
				 More groups might have been started by the Tester due to caching
				 (e.g. valid cache existed for all the Tests in the group 0 and so on)
				*/
//		    	final List<Test> nextIndependentTestsSet = this.tester.getAllTestsGroupedInOrder().get(nextGroupIndex);
				final ListIterator<List<Test>> nextIndependentTestslistIter = this.tester.getAllTestsGroupedInOrder().listIterator(nextGroupIndex);
//		    	this.tester.getCurrentlyIndependentTestsToFinish().set(nextIndependentTestsSet.size());
		    	
		    	boolean launchedNewTests = false;
		    	
		    	while(nextIndependentTestslistIter.hasNext()) {
		    		
		    		List <Test> nextIndependentTestslist = nextIndependentTestslistIter.next();
		    		this.tester.getCurrentlyIndependentTestsToFinish().set(nextIndependentTestslist.size());
		    		
					for (final Test testToRun : nextIndependentTestslist) {		// for the next set of independent tests
						/*
						 * The first group (at least) of independent Test is executed by the Tester itself.
						 * Here we are testing from (at least) the second group of independent tests onwards.
						 * So now all these test would have some dependencies with previous ones.
						*/
						try {
							
						if( this.isSingleTest == false && hasSatisfiedDependencies(testToRun) ){
							if (Thread.currentThread().isInterrupted()) {
								ers.Logger.debug(1, "Thread {" + Thread.currentThread().getName() + "} (SpecificTestRunnable.testFinished) was interrupted");
								unlinkAllAndRestoreInterrupted();
								return false;
							}
								SpecificTestResult cachedResult = this.tester.getTm().getCachedResult(this.tester.getCompCacheUID(), testToRun.UID());
								if(cachedResult != null) {
									SpecificTestRunnable.cachedTests.incrementAndGet();
									ers.Logger.debug(1, "there was a cached version for: " + this.tester.getCompCacheUID() + "@" + testToRun.UID());
									cachedResult.updateStdout("[This test result was a cached result]");
									this.tester.getCompResult().addResult(cachedResult);
									this.tester.getCurrentlyIndependentTestsToFinish().decrementAndGet();
								} else {
	//								System.out.println("No cached version found for:    " + this.tester.getCompCacheUID() + "@" + testToRun.UID());
									this.tester.getTm().getThreadPool().execute(new SpecificTestRunnable(testToRun, this.tester, this.isSingleTest ));
									launchedNewTests = true;
								}
							
						} else {
							this.tester.getCurrentlyIndependentTestsToFinish().decrementAndGet();
						}

						} catch (config.ConfigException | RejectedExecutionException ex) {
								ers.Logger.error( new ers.Issue(ex));
								return false;
						}

					}  // endof: for()
					
					if( launchedNewTests ) {
						break;
					}
					/*
					 * If we reach here it means that there were cached results for the whole current group.
					 * Adjust the counters and move (loop) too the next group (or finish, if that was the last group)
					 */
					if( this.tester.getGroupsToFinishNum().decrementAndGet() == 0 ) {
						this.tester.callUsersCallback();
						return true;
					}				
		    	}  // endof: while( there is a next group)
				/*
				 * Check whether at least one Test was given to the thread pool for execution.
				 * If not, it was due to unmet dependencies (or because there was a valid cached result for that Test)
				 *  and the testing of this component is done. So we call the callback!
				 */
				if( ! launchedNewTests ) {
					this.tester.callUsersCallback();
				}
				
			}
		} else {
			//System.out.println("   One Test done: " + this.runnable.getTest().UID() + ". There are still tests left");
		}
		return true;
	}

	
	/**
	 * The method checks the ExecOn* dependencies of a Test and returns a boolean stating whether those dependencies were satisfied.
	 * The dependencies are considered satisfied when:
	 * <ul>
	 * <li>All the ExecDependsOnSuccess were satisfied, i.e. all of those tests were TM_PASS</li>
	 * <b>OR</b>
	 * <li>At least one of the ExecDependsOnFailure was satisfied, i.e. at least one of those tests was NOT one of TM_UNDEF, TM_UNSUPPORTED, TM_PASS</li> 
	 * </ul>
	 * 
	 * @param test that has at least 1 ExecDependsOnSuccess or ExecDependsOnFailure dependency
	 * @return true if the given test has to be executed, false otherwise
	 */
	private boolean hasSatisfiedDependencies(Test test) throws config.ConfigException {
		
		boolean willExecute = true;
		boolean emptyDependsOnIntersectionTests4Comp = true;
		String unsatisfiedDependency = "";
		
		for ( final Test prevTest : test.get_ExecDependsOnSuccess() ) {
			if( ! this.tester.getTests4Comp().containsKey(prevTest.UID()) ) {	// Test may need to be ignored because of its has a scope or level
				continue;
			}
			emptyDependsOnIntersectionTests4Comp = false;
			SpecificTestResult prevResult = this.tester.getCompResult().getSpecificTestResult(prevTest.UID());
	
			if ( prevResult == null || prevResult.getTestReturnCode() != TestResult.TM_PASS.getInt() ) {
				unsatisfiedDependency = "Test " + test.UID() + " has ExecDependsOnSuccess dependency on the Test " + prevTest.UID()
						+ " but no result was found for the latter or the result was not TM_PASS. (prevResult=" + prevResult
						+ ") so Test " + test.UID() +  " will not be executed";
						
				ers.Logger.debug(5, new ers.Issue(unsatisfiedDependency + ", UNLESS ExecDependsOnFailure condition was met"));
				
				willExecute = false;
				break;
			}
		}
		
		if (willExecute && emptyDependsOnIntersectionTests4Comp == false) {
			ers.Logger.debug(5, new ers.Issue("Test " + test.UID() 
					+ " has ExecDependsOnSuccess dependencies that were satisfied (all of them TM_PASS), so it will be executed"));
			return true;		// all the Tests in ExecDependsOnSuccess were TM_PASS, so the Test must be executed
		}

		for ( final Test prevTest : test.get_ExecDependsOnFailure() ) {
			if( ! this.tester.getTests4Comp().containsKey(prevTest.UID()) ) {	// Test may need to be ignored because of its has a scope or level
				continue;
			}
			emptyDependsOnIntersectionTests4Comp = false;
			SpecificTestResult prevResult = this.tester.getCompResult().getSpecificTestResult(prevTest.UID());
			
			if ( prevResult != null
					&& prevResult.getTestReturnCode() != TestResult.TM_PASS.getInt() 
					&& prevResult.getTestReturnCode() != TestResult.TM_UNSUPPORTED.getInt()
					&& prevResult.getTestReturnCode() != TestResult.TM_UNDEF.getInt()) {
				ers.Logger.debug(5, new ers.Issue("Test " + test.UID() + " has ExecDependsOnFailure dependency on the Test " + prevTest.UID()
						+ " which was not TM_PASS, nor TM_UNSUPPORTED, nor TM_UNDEF, so Test " + test.UID() +  " will be executed."));
				unsatisfiedDependency = "";
				return true;		// there was at least a TM_FAIL in ExecDependsOnFailure, so the Test must be executed
			}
		}
		
	
		if( emptyDependsOnIntersectionTests4Comp ) {
			/*
			 * This will be the case when dependencies exist, but not in current scope/level,
			 * 		i.e. A -> B -> C and B is level 2 while A, C are level 0 (and we are testing will level 0); in this case B is ignored
			 */
//			ers.Logger.error(new ers.Issue("Test " + test.UID() + " was not in the first group of test, "
//					+ "and it did not have any ExecDependsOnFailure or ExecDependsOnSuccess dependencies,"
//					+ "i.e. it should have been in the first group of independant Tests!"));
			return true;
		}
		
		if ( unsatisfiedDependency.isEmpty() ) {
							// will not be executed because of unmet ExecDependsOnFailure
			unsatisfiedDependency = "Test " + test.UID() + " has ExecDependsOnFailure dependencies that were not met and will not be executed.";
		} else if (test.get_ExecDependsOnFailure().length != 0) {
			unsatisfiedDependency = "Moreover, it has ExecDependsOnFailure dependncies that were also not met";
							// will not be executed because of unmet ExecDependsOnSuccess AND unmet ExecDependsOnFailure too
		}
		ers.Logger.debug(5, new ers.Issue(unsatisfiedDependency));
		
		return false;
	}
	

	
	private void unlinkAllAndRestoreInterrupted() {
		Thread.currentThread().interrupt();
		for( Entry<String, Process> entry : this.tester.getTmHandle().getProcs().entrySet() ) {
			entry.getValue().unlink();
		}
	}

	
	private void setRepeatIfUnresolved(final boolean repeat) {
		this.repeatIfUnresolved = repeat;
	}

	
}
