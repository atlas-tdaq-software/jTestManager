package jTestManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


/**
 * 
 *  Describes the result for a component (Application or TestableObject in general).
 *  The TestResult accessible via the getComponentResult() is determined from the individual Tests' results by following the definitions here
 *  {@link https://twiki.cern.ch/twiki/bin/viewauth/Atlas/DaqHltTestManager#Test_Result_of_a_component_and_h}
 *
 */
public class CompResult {
	
    private final String componentID;
    private final List<Action> actions;
    private final ConcurrentMap<String, SpecificTestResult> resultsMap;

    private TestResult componentResult;
    private boolean hadUndef = false;
    private long timestamp;
    
    

	CompResult(String compID) {
		this.componentID = compID;
		this.resultsMap = new ConcurrentHashMap<>( 10, 0.9f, 1);
		this.componentResult = TestResult.TM_UNDEF;
		this.timestamp = System.currentTimeMillis();
		this.actions = Collections.synchronizedList(new ArrayList<Action>());
	}
	
	
	/**
	 * Adds a specific Test's result to the results map of the component and updates the component's Test Result accordingly 
	 * (see {@link https://twiki.cern.ch/twiki/bin/viewauth/Atlas/DaqHltTestManager#Test_Result_of_a_component_and_h } )
	 * 
	 * @param testResult the SpecificTestResult of one of the Tests for this component 
	 */
	synchronized void addResult( final SpecificTestResult testResult ) {
		
		this.resultsMap.put(testResult.getTestID(), testResult);
		this.timestamp = System.currentTimeMillis();
		
		/*
		 * if component result was already FAIL, it will always stay that way
		 * if the specific Test result was fail, then the component result is set to fail
		 * 
		 * For what 'fail' is as far as specific Tests are concerned see isFail() method of this class
		 */
		if ( this.componentResult == TestResult.TM_FAIL || isFail(testResult.getTestReturnCode()) ) {
			this.componentResult = TestResult.TM_FAIL;
			return;
		}
		
		/*
		 * At this point the componentResult can be: UNDEF, UNTESTED or PASS
		 * and the testResult can be: UNDEF, UNTESTED, UNRESOLVED or PASS
		 */
		
		if ( testResult.getTestReturnCode() == TestResult.TM_UNTESTED.getInt()
			|| testResult.getTestReturnCode() == TestResult.TM_UNRESOLVED.getInt()
			|| this.componentResult == TestResult.TM_UNTESTED ) {
			this.componentResult = TestResult.TM_UNTESTED;
			return;
		}
		
		/*
		 * At this point the componentResult can be: UNDEF  or PASS
		 * and the testResult can be: UNDEF or PASS
		 */
		
		if ( this.componentResult == TestResult.TM_PASS ) {
			if ( testResult.getTestReturnCode() == TestResult.TM_UNDEF.getInt() ) {
				this.hadUndef = true;
				this.componentResult = TestResult.TM_UNDEF;
				
			}
			// else the testResult is PASS, so we do not need to modify the component result, i.e. leave it TM_PASS
			return;
		}
		
		/*
		 * At this point the componentResult is: UNDEF
		 * and the testResult can be: UNDEF or PASS
		 */
		
		if ( testResult.getTestReturnCode() == TestResult.TM_PASS.getInt() ) {
			if ( ! this.hadUndef ) {
				this.componentResult = TestResult.TM_PASS;
			}
			// else leave it as is, i.e. componentResult == UNDEF
			return;
		}
		
		/*
		 * At this point the componentResult is: UNDEF
		 * and the testResult is: UNDEF
		 * 
		 * This if is added just for error checking
		 */

		if( this.componentResult == TestResult.TM_UNDEF && testResult.getTestReturnCode() == TestResult.TM_UNDEF.getInt() )  {
			this.hadUndef = true;
			// and the componentResult stays as is, i.e. UNDEF
			return;
		}
		
		// this should never be reached unless there was a mistake in the logic above
		ers.Logger.error(new ers.Issue("there is some error in the addResult method while adding testResult.getTestReturnCode()=" 
				+ testResult.getTestReturnCode() + ". Missed cases. Check the code"));
		
	}

	
	
	/**
	 * Retrieves the SpecificTestResult for a given Test for this component. Uses the {@link CompResult#getAllTestResults()}
	 * 
	 * @param testID the UID of the test
	 * @return the SpecificTestResult of the given Test, or null there was no such result for the component
	 */
	public SpecificTestResult getSpecificTestResult(final String testID) {
		return this.resultsMap.get(testID);
	}
	
	
	/**
	 * Returns the results of all the Test for this component.
	 * 
	 * @return A map with the mentioned results having the testIDs as keys and the results as values
	 */
	public ConcurrentMap<String, SpecificTestResult> getAllTestResults() {
		return this.resultsMap;
	}
	
	
	/**
	 * Uses a synchronizedList and it is imperative that the user manually synchronize on the returned list when iterating over it
	 * 
	 * @return All the Actions from all the Test of the component
	 */
	public List<Action> getActions() {
		return this.actions;
	}
	
	
	/**
	 * 
	 * @return The ID of the component that corresponds to this result
	 */
	public String getComponentID() {
		return this.componentID;
	}
	
	
	/**
	 * Returns the current result for the whole component. If testing is done, then this is final result of the component
	 * 
	 * @return the aforementioned result
	 */
	synchronized public TestResult getComponentResult() {
		return this.componentResult;
	}
	
	
	/**
	 * Returns the time on which the last SpecificTestResult was added. If there are not SpecificTestResult added, then the time of the object's creation is returned
	 * 
	 * @return the aforementioned result
	 */
	public long getTimestamp() {
		return this.timestamp;
	}
	
	
	/**
	 * Used only for setting the TM_UNSUPPORTED in case of no test.
	 */
	synchronized void setComponentResultToUnsupported() {
		this.timestamp = System.currentTimeMillis();
		this.componentResult = TestResult.TM_UNSUPPORTED;
	}
	
	
	/**
	 * Used only for setting to TM_FAIL in case of a hardware component that was disabled (state = false)
	 */
	synchronized void setComponentResultToHwDisabled() {
		this.timestamp = System.currentTimeMillis();
		this.actions.add(new Action("The component is OFF in the OKS database!"));
		this.componentResult = TestResult.TM_FAIL;
	}

	
	
	/**
	 * Checks if the given return code is some type of fail, ie checks whether it is not TM_UNTESTED, TM_UNRESOLVED, TM_PASS or TM_UNDEF
	 * 
	 * @param retCode The return code to be checked
	 * @return false if retCode is TM_UNTESTED, TM_UNRESOLVED, TM_PASS or TM_UNDEF, true otherwise
	 */
	static boolean isFail(int retCode) {
		if(  retCode != TestResult.TM_UNTESTED.getInt() 
				&& retCode != TestResult.TM_UNRESOLVED.getInt()
				&& retCode != TestResult.TM_PASS.getInt()
				&& retCode != TestResult.TM_UNDEF.getInt())
		{
			return true;
		} else {
			return false;
		}
	}
	
}
