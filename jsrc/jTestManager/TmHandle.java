package jTestManager;

import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import pmgClient.CorbaException;
import pmgClient.NoSuchPmgServerException;
import pmgClient.NonExistingProcessFile;
import pmgClient.PmgClientException;
import pmgClient.Process;

/**
 * 
 * The handle which gives the user control over the started Tests.
 * Users should avoid keeping references to handles that they no more need.
 *
 */
public class TmHandle {
	
	final private ConcurrentMap<String, pmgClient.Process > procs;
	final private String compID;

	
	TmHandle(String compID) {
		this.compID = compID;
		this.procs = new ConcurrentHashMap<>(4, 0.9f, 1);
	}
	

//	public boolean isItFinished(){
//		return true;
//	}

	/**
	 * Will stop (call pmg stop) all the process of the Tests of the component
	 */
	public void stop() {
		for( Entry<String, Process> entry : procs.entrySet() ) {
			try {
				entry.getValue().stop();
				entry.getValue().unlink();
			} catch (PmgClientException ex) {
				ers.Logger.error(new ers.Issue("Exception while stopping testing process of app [" 
						+ entry.getValue().getHandle().getAppName() + "] launched by the testing of component ["
						+ this.compID + "]", ex));
			}
		}
	}
	

	/**
	 * Returns a String with the stdout of all the processes related to this ccomponent under test in the format:
	 * App name: [name] Output: [out] \n\n
	 * 
	 * @return The aforementioned String
	 */
	public String getRuntimeStdout() {
		StringBuilder strB = new StringBuilder(256);
		for( Entry<String, Process> entry : procs.entrySet() ) {
			try {
				strB.append("App name: ");
				strB.append(entry.getValue().getHandle().getAppName());
				strB.append("  Output: ");
				strB.append(entry.getValue().outFileStr());
				strB.append("\n\n");
			} catch (NoSuchPmgServerException | NonExistingProcessFile | CorbaException e) {
				ers.Logger.error(new ers.Issue("Exception while getting the stdout of process of app [" 
						+ entry.getValue().getHandle().getAppName() + "] launched by the testing of component ["
						+ this.compID + "]", e));
			}
		}
		return strB.toString();
	}
	
	
	/**
	 * Returns a String with the stdout of all the processes related to this component under test in the format:
	 * App name: [name] Output: [out] \n\n
	 * 
	 * @return
	 */
	public String getRuntimeStderr() {
		StringBuilder strB = new StringBuilder(256);
		for( Entry<String, Process> entry : procs.entrySet() ) {
			try {
				strB.append("App name: ");
				strB.append(entry.getValue().getHandle().getAppName());
				strB.append("  Output: ");
				strB.append(entry.getValue().errFileStr());
				strB.append("\n\n");
			} catch (NoSuchPmgServerException | NonExistingProcessFile | CorbaException e) {
				ers.Logger.error(new ers.Issue("Exception while getting the stderr of process of app [" 
						+ entry.getValue().getHandle().getAppName() + "] launched by the testing of component ["
						+ this.compID + "]", e));
			}
		}
		return strB.toString();
	}
	
	
	/**
	 * Adds a process to the map of processes for this component-testing
	 * 
	 * Currently the key is:
	 * tester.getBaseclassName() + "@" + tester.getCompID() + "@" + test.UID() + "@" + appName;
	 * 
	 * @param key a key for the process as mentioned
	 * @param proc the process obj
	 */
	void addProcess(final String key, final pmgClient.Process proc) {
		this.procs.put(key, proc);
	}
	
	/**
	 * Removes a process from the map of processes for this component-testing
	 * Currently the key is:
	 * tester.getBaseclassName() + "@" + tester.getCompID() + "@" + test.UID() + "@" + appName;
	 * 
	 * @param key a key for the process as mentioned
	 */
	void removeProcess(final String key) {
		this.procs.remove(key);
	}
	
	/**
	 * 
	 * @return a Map with all the processes related to the component under test
	 */
	ConcurrentMap<String, pmgClient.Process> getProcs() {
		return this.procs;
	}

}
