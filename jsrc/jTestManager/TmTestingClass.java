package jTestManager;

import java.util.Arrays;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import TM.Executable;
import TM.ExecutableTest;
import TM.ExecutableTest_Helper;
import TM.Test;
import TM.Test.Scope;
import config.Configuration;
import dal.BaseApplication;
import dal.BaseApplication_Helper;
import dal.Partition;
import dal.TestableObject;
import dal.TestableObject_Helper;

public class TmTestingClass {
	

	private static CountDownLatch cbToGet;
	private static AtomicInteger unsupportedCount = new AtomicInteger(0);
	private static AtomicInteger tmCBCount = new AtomicInteger(0);
	private static AtomicInteger passCount = new AtomicInteger(0);
	private static AtomicInteger failCount = new AtomicInteger(0);
	private static AtomicInteger untestedCount = new AtomicInteger(0);
	private static AtomicInteger unresolvedCount = new AtomicInteger(0);
	private static AtomicInteger undefCount = new AtomicInteger(0);
	private static AtomicInteger otherCount = new AtomicInteger(0);
	
	private static boolean onlyPrintDetailsForFails = false;
	private static boolean skipStreams = false;
	
	
	private static ConcurrentHashMap<String, String> map;
	
	
	public class TmCallbackQuick implements jTestManager.Callback {

		@Override
		public void componentTestDone(CompResult compResult) {
			System.out.println("Quick callback....! ");

			cbToGet.countDown();
		}
	}
	
	public class TmCallbackQuick_2 implements jTestManager.Callback {

		@Override
		public void componentTestDone(CompResult compResult) {
			System.out.println("Quick callback 2 ....! 2 ");

			cbToGet.countDown();
		}
	}
	
	
	public class TmCallback implements jTestManager.Callback {
		
		@Override
		public void componentTestDone(CompResult compResult) {
			tmCBCount.incrementAndGet();
			
			/*
			 * Things here are not very well coded but...
			 */
			
			StringBuilder strB = new StringBuilder(100);
			strB.append("\n\tIndividual Tests results:\n");
			for ( Entry<String, SpecificTestResult> e :compResult.getAllTestResults().entrySet()){
				strB.append("\t\tTest: ");
				strB.append(e.getKey());
				
				strB.append("\t\tResult: ");
				switch (e.getValue().getTestReturnCode()) {
				case 0:
					strB.append("TM_PASS");
					break;
				case 183:
					strB.append("TM_FAIL");
					break;
				case 184:
					strB.append("TM_UNRESOLVED");
					break;
				case 185:
					strB.append("TM_UNTESTED");
					break;
				case 186:
					strB.append("TM_UNSUPPORTED");
					System.err.println("TM_UNSUPPORTED for a specific Test(!) This applies only to components");
					break;
				case 182:
					strB.append("TM_UNDEF");
					System.err.println("TM_UNDEF for test: " + e.getKey() + " of the component: " + compResult.getComponentID());
					break;
				default:
					strB.append("Custom FAIL " + e.getValue().getTestReturnCode());
				}
				
				if( !skipStreams ) {
					strB.append(" \tstdout: >" + e.getValue().getStdout());
					strB.append("< \tstderr: >" + e.getValue().getStderr() );
					
					strB.append("<\n");
				} else {
					strB.append("\n");
				}
			}
			
			if (compResult.getComponentResult() != TestResult.TM_PASS) {
				strB.append("\tActions:: ");
				for( Action a : compResult.getActions()) {
					if ( a instanceof ExecAction ) {
						for ( Exec exec : ((ExecAction)a).getExecs() ) {
							strB.append("    computerProgram: " + exec.program.UID());
							strB.append(" cmdLinePars: " + exec.cmdLinePars);
						}
					} else if ( a instanceof RebootAction ) {
						strB.append(" RebootAction: ");
						strB.append(" host UID: " + ((RebootAction)a).getHost().UID() );
					} else if ( a instanceof TestApplicationAction ) {
						strB.append(" TestApplicationAction: ");
						strB.append(" Appid: " + ((TestApplicationAction)a).getApplication().UID() );
					} else if (a instanceof TestObjectAction ) {
						strB.append(" TestObjectAction: ");
						strB.append(" UID: " + ((TestObjectAction)a).getTestableObject().UID() );
					} else {
						strB.append(" Other Action: ");
					}
					strB.append(" Dianosis: " + a.getDiagnosis());
				}
				strB.append("\n");
			}
			
			
//			final boolean filterOutUnsupported = true;
//			final boolean filterOutPassed = true;
//			if((filterOutUnsupported && compResult.getComponentResult() == TestResult.TM_UNSUPPORTED)
//				|| (filterOutPassed &&  compResult.getComponentResult() == TestResult.TM_PASS)) {
//				return;
//			}


			switch (compResult.getComponentResult().getInt() ) {
			case 0:
				passCount.incrementAndGet();
				break;
			case 183:
				failCount.incrementAndGet();
				break;
			case 184:
				unresolvedCount.incrementAndGet();
				break;
			case 185:
				untestedCount.incrementAndGet();
				break;
			case 186:
				unsupportedCount.incrementAndGet();
				break;
			case 182:
				undefCount.incrementAndGet();
				break;
			default:
				otherCount.incrementAndGet();
			}
			
			StringBuilder finalStr = new StringBuilder();
			
			finalStr.append( "--- TmCallback:   compResult:" );
			finalStr.append( compResult.getComponentResult() );
			finalStr.append( "\tcallbacks to wait for: " );
			cbToGet.countDown();
			finalStr.append( cbToGet.getCount() );
			finalStr.append( "\t\tcomponentID: " );
			finalStr.append( compResult.getComponentID() );
			
			if( onlyPrintDetailsForFails ) {
				if ( compResult.getComponentResult() == TestResult.TM_FAIL ) {
					finalStr.append(strB.toString());
				}
			} else {
				finalStr.append(strB.toString());
			}
			
			System.out.println(finalStr.toString());
					
			if( map.remove(compResult.getComponentID()) == null ) {
				System.err.println(" -+- key : " + compResult.getComponentID() + " did not exist in the map! *** ");
			}

			
		}	// endof componentTestDone() method

	} // endof: TmCallback class

	
	
	
	
	

	public static void main(String[] args) {
		
		
		boolean validArgs = true;
		boolean doTestables = false; 	// true for TestableObjects, false BaseApplication
		boolean getTestsOnly = false;
		String specificApp = null;
		String specificTestableObj = null;
		int level = Tm.DEFAULT_LEVEL;
		Scope scope = Tm.DEFAULT_SCOPE;
		String db = null;
		String partitionName = null;
		map = new ConcurrentHashMap<String, String>();
		
		System.out.println("");
		
		for ( int i=0; i<args.length; i++ ) {
			String arg = args[i];
			switch (arg) {
			case "-d":
				if( i+1 < args.length && ! args[i+1].startsWith("-") ) {
					db = args[++i];
				} else {
					validArgs = false;
					System.err.println("Argument '-d' must be followed by a database file");
				}
				break;
			case "-p":
				if( i+1 < args.length && ! args[i+1].startsWith("-") ) {
					partitionName = args[++i];
				} else {
					validArgs = false;
					System.err.println("Argument '-p' must be followed by a partition name");
				}
				break;
			case "-a":
				doTestables = false;
				if( i+1 < args.length && ! args[i+1].startsWith("-") ) {
					specificApp = args[++i];
				}
				break;
			case "-t":
				doTestables = true;
				if( i+1 < args.length && ! args[i+1].startsWith("-") ) {
					specificTestableObj = args[++i];
				}
				break;
			case "--find-tests":
				getTestsOnly = true;
				break;
			case "--skip-streams":
				skipStreams = true;
				break;
			case "--fail-only-details":
				onlyPrintDetailsForFails = true;
				break;
			case "-s":
				if( i+1 < args.length && ! args[i+1].startsWith("-") ) {
					switch(args[++i]) {
					case "any":
						scope = Scope.Any;
						break;
					case "diagnostics":
						scope = Scope.Diagnostics;
						break;
					case "functional":
						scope = Scope.Functional;
						break;
					case "precondition":
						scope = Scope.Precondition;
						break;
					default:
						validArgs = false;
						System.err.println("Scope must be one of the following: any, diagnostics, functional, precondition");
						break;
					}
				} else {
					validArgs = false;
					System.err.println("Scope must be one of the following: any, diagnostics, functional, precondition");
				}
				break;
			case "-l":
				if( i+1 < args.length && ! args[i+1].startsWith("-") ) {
					try {
						level = Integer.parseInt(args[++i]);
						if( level > 3 || level < 0 ) {
							validArgs = false;
							System.err.println(" Level out of bounds! The level must be: 0, 1, 2 or 3, valid example: -l 1");
						}
					} catch(NumberFormatException e) {
						validArgs = false;
						System.err.println("Level not a number! Must be: 0, 1, 2 or 3, valid example: -l 1");
					}
				} else {
					validArgs = false;
					System.err.println("No level given! The level must be: 0, 1, 2 or 3, valid example: -l 1");
				}
				break;
			case "-h":
				validArgs = false;
				break;
			default:
				System.err.println("Wrong argument " + arg);
				validArgs = false;
				break;
			}
		} // for args

		if ( !validArgs ||  db == null || partitionName == null ) {
			System.out.println("\nUsage: -d partition -p partitionName [OPTIONS] \n\n \t\t If no -a or -t was given, will test all BaseApplications\n");
			System.out.println("\t -d partition        : partition as oksconfig:path_to_database_file OR rdbconfig:RDB@Partition_name");
			System.out.println("\t -p partitionName    : partition name");
			System.out.println("\n\t OPTIONS:\n");
			System.out.println("\t -a [appID]             : tests the given BaseApplication or all the applications if unspecified (default)");
			System.out.println("\t -t [testableObject ID] : tests the given testable object (non app) or all of them if unspecified");
			System.out.println("\t --fail-only-details    : print details only for fails");
			System.out.println("\t --skip-streams         : do not print stdout and stderr");
			System.out.println("\t --find-tests           : Used in combination with -a/t [ID] to return the tests (and execs) without actually running any of them."
							 + "\n                            Note: for BaseApplications the disabled are not shown. For testable objects everything is shown.");
			System.out.println("\t -s Scope      : set the scope for the tests to execute/show, available scopes: any, diagnostics, functional, precondition . Default: any");
			System.out.println("\t -l level      : set the level (0, 1, 2 or 3) for the tests to execute/show. Default: max level (255)");
			System.out.println("\t -h            : shows this help and exits");
			System.out.println("\t The interlan thread pool size can be adjusted by setting TDAQ_TM_TPOOL_SIZE (default 16) ");
			
			return;
		}
		

		
		System.out.println("\n Running in TDAQ_VERSION: " + System.getenv("TDAQ_VERSION") + "\n") ;
		
		
		Configuration config = null;

		
		try {
			config = new Configuration(db);
		} catch (config.SystemException ex) {
			System.err.println("Caught \'config.SystemException\':\n" + ex.getMessage());
			return;
		}
		
		Partition partition = null;
		
		try { 
			partition = dal.Algorithms.get_partition(config, partitionName);
		config.register_converter(new dal.SubstituteVariables(partition));		// this call replaces parameters of format ${x}
		
		if (partition == null) {
			System.err.println("Some error while getting the partition from dal: " + partitionName);
			return;
		}
		System.out.println("Got the Partition object for the partition: " + partitionName);
		
		
		BaseApplication[] appArray = partition.get_all_applications(null, null, null);
		
		TestableObject[] testObjArray = TestableObject_Helper.get(config, new config.Query(""));

		
		System.out.println("\n-----------------------------------------------------------------");
		
		Tm tm = null;
		try {
			tm = new Tm(config, partitionName);
		} catch (config.ConfigException | NoTestHostExcepion e) {
			ers.Logger.fatal(new ers.Issue("Exception during tm creation!", e));
		} catch (WrongConfigurationException e) {
			ers.Logger.fatal(new ers.Issue("Exception during tm creation!", e));
		}

		
		tm.setDiscardOutput(skipStreams);
			
//		try {
//			System.out.println("Reloading...");
//			tm.reload(db, "lyk");
//			System.out.println("Reloaded");
//		} catch (NoTestHostExcepion e1) {
//			e1.printStackTrace();
//		}

		System.out.println("-----------------------------------------------------------------");
		System.out.println("---   using scope: " + scope.toString() );
		System.out.println("---   using level: " + level );
		System.out.println("---   Components are: " + (doTestables? "TestableObjects" : "BaseApplications") );
		System.out.println("---   ThreadPool size: " + tm.getThreadPoolSize() );
		System.out.println("---   Test hosts number: " + tm.getTestHosts().size() );
		System.out.println("---   PartitionName: " + partitionName );
		System.out.println("-----------------------------------------------------------------");

		
		/*
		 *  Part for showing the tests without executing them
		 *  (not the best way to do it, but ok...)
		 */
		
		if ( getTestsOnly && doTestables ) {	// for TestableObjects
			for( TestableObject testableObj : testObjArray  ) {
				if( BaseApplication_Helper.cast(testableObj) == null ) {
					if ( specificTestableObj != null && ! specificTestableObj.equals(testableObj.UID() ) ) {
						continue;
					}
					Map<String, Map<String, Test>> testsMap = tm.getTestsForTestableObjects(Arrays.asList(new TestableObject[]{testableObj}), scope, level);
					Map<String, Test> tests4Comp = testsMap.get(testableObj.UID());
					printTestsAndExecs(tests4Comp, testableObj.UID());
				}
			}
			return;
		} else if ( getTestsOnly ) {	//for BaseApplications
			for ( BaseApplication app : appArray) {
				if ( specificApp != null && ! specificApp.equals(app.UID() ) ) {
					continue;
				}
				Map<String, Map<String, Test>> testsMap = tm.getTestsForApps(Arrays.asList(new BaseApplication[]{app}), scope, level);
				Map<String, Test> tests4Comp = testsMap.get(app.UID());
				printTestsAndExecs(tests4Comp, app.UID());
			}
			return;
		}

		TmHandle handle = null;
		
		
		for(int i=0; i<1; i++ ) {		// loop to be used in order to Test caching
			
			jTestManager.Callback cb = (new TmTestingClass()). new TmCallback();
			
			long startTime;
			
			int compsToTest = 0;
			/*
			 * TestableObjects that are not BaseApplications
			 */
			
			if(doTestables) {
				for( TestableObject testableObj : testObjArray  ) {
					if( BaseApplication_Helper.cast(testableObj) == null ) {
						
						if( specificTestableObj != null && ! testableObj.UID().equals(specificTestableObj)) {
							continue;
						}
						
						compsToTest++;
					} 
				}
	
				cbToGet = new CountDownLatch(compsToTest );
				
				startTime = System.currentTimeMillis();
	
				for( TestableObject testableObj : testObjArray  ) {
					if( BaseApplication_Helper.cast(testableObj) == null ) {
						if( specificTestableObj != null && ! testableObj.UID().equals(specificTestableObj)) {
							continue;
						}
						
	//					System.out.println("* Testing TestableObject : " + testableObj.UID());
						if( map.put(testableObj.UID(), testableObj.UID()) != null ) {
							System.err.println(" objectUID: " + testableObj.UID() + " was already in the map!");
						}
						
						try {
							handle = tm.test(testableObj, cb, scope, level);
						} catch (NoTestHostExcepion | WrongConfigurationException e) {
							ers.Logger.fatal(e);
							e.printStackTrace();
						}
					} 
				}
				
			} else {
				/*
				 * BaseApplications 
				 */
				
				for ( BaseApplication app : appArray) {
					if( specificApp != null && ! app.UID().equals(specificApp)) {
						continue;
					}
					compsToTest++;
				}
				
				cbToGet = new CountDownLatch(compsToTest);
				
				startTime = System.currentTimeMillis();
	
				for ( BaseApplication app : appArray ) {
					if( specificApp != null && ! app.UID().equals(specificApp)) {
						continue;
					}
					//System.out.println("* Testing AppConfig : " + appConfig.get_app_id() );
					map.put(app.UID(), app.UID());
					
					try {
						handle = tm.test(app, cb, scope, level);					
					} catch (WrongConfigurationException e) {
						ers.Logger.fatal(new ers.Issue("WrongConfigurationException for app " + app.UID() + "............!", e));
						e.printStackTrace();
					}
				}
				
			}		
			
					
			
			try {
				while ( ! cbToGet.await(4, TimeUnit.SECONDS)) {
					long endTime = System.currentTimeMillis();
					System.out.println("\n\n   ^^^ remaining comp Tests: " + cbToGet.getCount() + " out of " + compsToTest + " ^^^"
	//						+ "\n  REAL CBS count       : " + PmgClient.REAL_PMG_CBS.get()
	//						+ "\n  REAL CBS SERVER count: " + PmgClient.REAL_PMG_CBS_SERVER.get()
							+ "\n  TM cbs count         : " + tmCBCount.get()
							+ "\n  runnable PMG ALL CBS   : " + SpecificTestRunnable.pmgALLCBS.get()
							+ "\n  pmgProcessesStarted  : " + SpecificTestRunnable.pmgProcessesStarted.get()
							+ "\n  pmgNON_RUN_CBs       : " + SpecificTestRunnable.pmgNON_RUN_CbsReceived.get()
							+ "\n  pmgRunningReceived   : " + SpecificTestRunnable.pmgRunningReceived.get()
							+ "\n  pmgRunAssummed       : " + SpecificTestRunnable.pmgRunningAssumed.get()
							+ "\n  pmgCbIsFailOrSyncerror : " + SpecificTestRunnable.pmgCbIsFailOrSyncerror.get()
							+ "\n  pmgStartExceptions     : " + SpecificTestRunnable.exceptionsCaught.get()
							+ "\n  pmgCbButProcNotInTheMap: " + SpecificTestRunnable.pmgCbButProcNotInTheMap.get()
							+ "\n  testFinishedNormal       : " + SpecificTestRunnable.testFinishedNormal.get()
							+ "\n  testFinishedZeroLaunched : " + SpecificTestRunnable.testFinishedZeroLaunched.get()
							+ "\n  testFinishedCORBA        : " + SpecificTestRunnable.testFinishedCORBA.get()
							+ "\n\n  CachedTests        : " + SpecificTestRunnable.cachedTests.get()
							+ "\n\n  passCount:        " + passCount.get()
							+ "\n  failCount:        " + failCount.get()
							+ "\n  untestedCount:    " + untestedCount.get()
							+ "\n  unsupportedCount: " + unsupportedCount.get()
							+ "\n  undefCount:       " + undefCount.get()
							+ "\n  unresolvedCount:  " + unresolvedCount.get() + " (should be 0 always)"
							+ "\n  otherCount:       " + otherCount.get() + " (should be 0 always)"
							+ "\n  time: " + (endTime-startTime)/1000 + " secs");
					
//					System.out.println("Processes map of pmgClient: ");
//					for( Entry<pmgClient.Handle, pmgClient.Process> e : tm.getPmg().processesMap.entrySet() ) {
//						System.out.println("pmg key: " + e.getKey() + " \tvalue : "+ e.getValue().getProcessStatusInfo().state.toString());
//					}
//					System.out.println("\nProcesses map in TmTestingClass (added before tm.test call)");
//					for( Entry<String, String> e : map.entrySet() ) {
//						System.out.println("tmTest key: " + e.getKey() + " \tvalue : "+ e.getValue());
//					}
				
					
//					System.out.println("\n\n Stdout of the component Test last launched : " + handle.getRuntimeStdout() );
//					System.out.println("\n\n Stderr of the component Test last launched : " + handle.getRuntimeStderr() + "\n\n" );
//					handle.stop();				
					
				} 
			} catch (InterruptedException e) {
				System.err.println("InterruptedException");
			}
			long endTime = System.currentTimeMillis();
		
	
	
	
			System.out.println("\n ------- Done -------");
			System.out.println("\n   ^^^ remaining comp Tests: " + cbToGet.getCount() + " out of " + compsToTest + " ^^^"
	//				+ "\n  REAL CBS count       : " + PmgClient.REAL_PMG_CBS.get()
	//				+ "\n  REAL CBS SERVER count: " + PmgClient.REAL_PMG_CBS_SERVER.get()
					+ "\n  TM cbs count         : " + tmCBCount.get()
					+ "\n  runnable PMG ALL CBS   : " + SpecificTestRunnable.pmgALLCBS.get()
					+ "\n  pmgProcessesStarted  : " + SpecificTestRunnable.pmgProcessesStarted.get()
					+ "\n  pmgNON_RUN_CBs       : " + SpecificTestRunnable.pmgNON_RUN_CbsReceived.get()
					+ "\n  pmgRunningReceived   : " + SpecificTestRunnable.pmgRunningReceived.get()
					+ "\n  pmgRunAssummed       : " + SpecificTestRunnable.pmgRunningAssumed.get()
					+ "\n  pmgCbIsFailOrSyncerror : " + SpecificTestRunnable.pmgCbIsFailOrSyncerror.get()
					+ "\n  pmgStartExceptions     : " + SpecificTestRunnable.exceptionsCaught.get()
					+ "\n  pmgCbButProcNotInTheMap: " + SpecificTestRunnable.pmgCbButProcNotInTheMap.get()
					+ "\n  testFinishedNormal       : " + SpecificTestRunnable.testFinishedNormal.get()
					+ "\n  testFinishedZeroLaunched : " + SpecificTestRunnable.testFinishedZeroLaunched.get()
					+ "\n  testFinishedCORBA        : " + SpecificTestRunnable.testFinishedCORBA.get()
					+ "\n\n  CachedTests        : " + SpecificTestRunnable.cachedTests.get()
					+ "\n\n  passCount:        " + passCount.get()
					+ "\n  failCount:        " + failCount.get()
					+ "\n  untestedCount:    " + untestedCount.get()
					+ "\n  unsupportedCount: " + unsupportedCount.get()
					+ "\n  undefCount:       " + undefCount.get()
					+ "\n  unresolvedCount:  " + unresolvedCount.get() + " (should be 0 always)"
					+ "\n  otherCount:       " + otherCount.get() + " (should be 0 always)"
					+ "\n  ====================================== "
					+ "\n  Components Tested: " + compsToTest
					+ "\n  Time:              " + (endTime-startTime)/1000. + " secs\n");
			System.out.println("  Scope:              " + scope.toString() );
			System.out.println("  Level:              " + level );
			System.out.println("  Components are:     " + (doTestables? "TestableObjects" : "BaseApplications") );
			System.out.println("  ThreadPool size:    " + tm.getThreadPoolSize() );
			System.out.println("  Test hosts number:  " + tm.getTestHosts().size() );
			System.out.println("\n  ====================================== ");
			System.out.println("\n ------- Done -------");
	
			try { Thread.sleep(1000); } catch(InterruptedException e) {}
			resetCounters();
//			for( TestableObject obj : testObjArray ) {
//				 tm.reset(obj);
//			}
//			for( BaseApplication appConf : appConfigArray ) {
//				 tm.reset(appConf);
//			}
		}
		
		tm.shutdown();

		} catch ( final config.ConfigException ex ) { 
			ers.Logger.error(ex) ;
			return ;
		}

	}
	

	private static void resetCounters() {
		unsupportedCount = new AtomicInteger(0);
		tmCBCount = new AtomicInteger(0);
		passCount = new AtomicInteger(0);
		failCount = new AtomicInteger(0);
		untestedCount = new AtomicInteger(0);
		unresolvedCount = new AtomicInteger(0);
		undefCount = new AtomicInteger(0);
		otherCount = new AtomicInteger(0);
		
		SpecificTestRunnable.pmgNON_RUN_CbsReceived = new AtomicInteger(0);
		SpecificTestRunnable.pmgRunningReceived = new AtomicInteger(0);
		SpecificTestRunnable.pmgRunningAssumed = new AtomicInteger(0);
		SpecificTestRunnable.pmgProcessesStarted = new AtomicInteger(0);
		SpecificTestRunnable.exceptionsCaught = new AtomicInteger(0);
		SpecificTestRunnable.pmgALLCBS = new AtomicInteger(0);
		SpecificTestRunnable.pmgCbIsFailOrSyncerror = new AtomicInteger(0);
		SpecificTestRunnable.pmgCbButProcNotInTheMap = new AtomicInteger(0);
		SpecificTestRunnable.cachedTests = new AtomicInteger(0);

		SpecificTestRunnable.testFinishedNormal = new AtomicInteger(0);
		SpecificTestRunnable.testFinishedZeroLaunched = new AtomicInteger(0);
		SpecificTestRunnable.testFinishedCORBA = new AtomicInteger(0);
	}
	
	
	private static void printTestsAndExecs(Map<String, Test> tests4Comp, String uid) {
		
		StringBuilder strB = new StringBuilder(128);
		strB.append("------------ Tests for component: " + uid);
		try {
		for( Map.Entry<String, Test> e : tests4Comp.entrySet()) {
			strB.append("\n\tTest ID:  ");
			strB.append(e.getKey());
			strB.append("  scope: " );
			for( String s : e.getValue().get_Scope()) {
				strB.append(s + ", ");
			}
			strB.append("  level: " + e.getValue().get_Complexity() );
			
			ExecutableTest execTest = ExecutableTest_Helper.cast(e.getValue());
			if( execTest != null ) {
				strB.append("\n\t    Binaries: ");
				for( Executable exec : execTest.get_Runs() ) {
					strB.append( "\n\t        " + exec.get_Executes().get_BinaryName() ) ;
				}
			}
		}
		} catch ( final config.ConfigException ex ) { 
			ers.Logger.error(ex) ;
			return ;
		}
		
		System.out.println(strB.toString() + "\n");
	}
	

}
