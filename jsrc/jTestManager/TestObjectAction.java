package jTestManager;

import dal.TestableObject;


/**
 * 
 * The Action corresponding to the failure TestUponFailure in case the component to be tested is a TestableObject
 *
 */
public class TestObjectAction extends Action {

	private TestableObject testableObj;
	
	public TestObjectAction(TestableObject testableObj, String diagnosis) {
		super(diagnosis);
		this.testableObj = testableObj;
	}
	
	/**
	 * 
	 * @return The TestableObject to be tested
	 */
	public TestableObject getTestableObject() {
		return this.testableObj;
	}

}
