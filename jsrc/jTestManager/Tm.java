package jTestManager;

import jTestManager.Cache.CachedSpecificTestResult;

import java.lang.Thread.UncaughtExceptionHandler;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import ers.Issue;
import pmgClient.PmgClient;
import config.ConfigObject;
import config.Configuration;
import config.NotFoundException;
import dal.BaseApplication;
import dal.Computer;
import dal.ComputerBase;
import dal.ComputerSet;
import dal.ComputerSet_Helper;
import dal.Computer_Helper;
import dal.HW_Object;
import dal.HW_Object_Helper;
import dal.Partition;
import dal.Segment;
import dal.TestRepository;
import dal.TestableObject;
import TM.Test;
import TM.Test.Scope;
import TM.Test4CORBAServant;
import TM.Test4CORBAServant_Helper;
import TM.Test4Class;
import TM.Test4Class_Helper;
import TM.Test4Object;
import TM.Test4Object_Helper;
import TM.TestSet;
import TM.TestSet_Helper;


/**
 * 
 * The actual class that represents the TestManager.
 * Only one instance of this class is needed and the @link {@link Tm#shutdown()} method needs to be called before program exits 
 *
 */
public class Tm {

	final public static int DEFAULT_LEVEL = 255;
	final public static int MAX_LEVEL = 255;
    final public static Scope DEFAULT_SCOPE = Scope.Any;

	final private String partitionName;
	final private PmgClient pmg;
	final private ReentrantReadWriteLock readWriteLock;	
	final private Lock readLock;
	final private Lock writeLock;
	final private Map<String, BaseApplication> appMap;
	final private List<Test> allTestsSet;
	
	volatile private ExecutorService threadPool;
     
    private TreeMap<String, TreeSet<String>> superClassesMap;
	private Configuration config;
	private Partition partition;	
	private List<Computer> testHosts;
	private Cache cache;
    private int threadPoolSize = 16;			// just a default value
    private AtomicBoolean isVerbose;
    private AtomicBoolean discardOutput;
    
    
   /**
    * Internal ThreadFactory class used in all the Threads of the Thread pool
    *
    */
   private static class TmThreadFactory implements ThreadFactory {
		@Override
		public Thread newThread(Runnable r) {
			Thread thread = Executors.defaultThreadFactory().newThread(r);
			
			thread.setUncaughtExceptionHandler( new UncaughtExceptionHandler() {
				@Override
				public void uncaughtException(Thread t, Throwable e) {
					ers.Logger.error(new ers.Issue("Uncaught Exception in thread {" + t.getName()
							+ "} of the thread pool (caught by custom UncaughtExceptionHandler)", e));
					e.printStackTrace();
				}
			});
			return thread;
		}
	}

    
    
    /**
     * Same as {@link Tm#Tm(Configuration, String, PmgClient)} without providing a Process Manager client (a new one is created from the TM)
     */
    public Tm(final Configuration conf, final String partitionName)
	throws NoTestHostExcepion, WrongConfigurationException, config.SystemException, config.NotFoundException, config.NotValidException, config.GenericException, WrongConfigurationException {
    	this(conf, partitionName, null);
    }
    

    /**
     * Constructs a Test Manager object. 
     * 
     * @param conf A proper configuration object (make sure register_converter was called on it)
     * @param partitionName the name on the Partition
     * @param pmgClient A Process Manager Client Object
     * @throws NoTestHostExcepion when there was some problem in getting TestHosts
     * @throws WrongConfigurationException when there was some problem wit the configuration
     */
    public Tm(final Configuration conf, final String partitionName, final PmgClient pmgClient)
	throws NoTestHostExcepion, WrongConfigurationException, config.SystemException, config.NotFoundException, config.NotValidException, config.GenericException, WrongConfigurationException {
    	this.isVerbose = new AtomicBoolean(false);
    	this.discardOutput = new AtomicBoolean(false);
    	this.config = conf;
    	this.partitionName = partitionName;
    	try {
    		this.partition = dal.Algorithms.get_partition(config, partitionName);
    		BaseApplication[] appArray = this.partition.get_all_applications(null, null, null);
    		this.appMap = new HashMap<>();
    		for(BaseApplication app : appArray) {
    			this.appMap.put(app.UID(), app);
//    			System.out.println("app id:" + app.get_app_id() + " map size: " + appConfArray.length);
    		}
    	} catch (config.SystemException | config.NotFoundException ex) {
    		throw new WrongConfigurationException(ex);
    	}
		

		
		this.superClassesMap = this.config.superclasses(); // Get superclasses of each class
		for ( Entry<String, TreeSet<String>> e : this.superClassesMap.entrySet()) {
			e.getValue().add(e.getKey());			// add the class itself to the set of its superclasses
		}

		
		this.readWriteLock = new ReentrantReadWriteLock();
		this.readLock = this.readWriteLock.readLock();
		this.writeLock = this.readWriteLock.writeLock();
		
		this.allTestsSet = new LinkedList<>();
		addTests(allTestsSet, this.partition);	
		addTests(allTestsSet, this.partition.get_OnlineInfrastructure());
		

		// Attach a pmg client
		
		if( pmgClient != null ) {
			this.pmg = pmgClient;		// user of Tm library provided a pmg himself
		} else {
			try {
				this.pmg = new PmgClient();		// or else create a new one
			} catch (pmgClient.ConfigurationException e) {
				throw new RuntimeException();
			}	
		}
		
		
		// Add the TestHosts
		
		ComputerSet computerSet = this.partition.get_OnlineInfrastructure().get_TestHosts();
		testHosts = (ArrayList<Computer>) getAllComputers(computerSet);
		
		if (testHosts.isEmpty()) {		// If there were none TestHosts, add the default Host
			Computer pc = this.partition.get_DefaultHost();
			if( pc != null && pc.get_State() == true ) {
				testHosts.add(pc);
			}
		}
		
		if (testHosts.isEmpty()) {		// If there was no DefaultHost for the partition, add the default Host
			try {
				java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
				ConfigObject localhostConfigObj = this.config.get_object("Computer", localMachine.getCanonicalHostName());
				
				Computer localhostComputer = Computer_Helper.get(this.config, localhostConfigObj);
				if( localhostComputer != null && localhostComputer.get_State() == true ) {
					testHosts.add(localhostComputer);
				}
			} catch (UnknownHostException ex) {
				throw new NoTestHostExcepion("No testHosts found in the database and the IP address of localhost could not be determined.", ex); 
			} catch (NotFoundException ex) {
				throw new NoTestHostExcepion("No testHosts found in the database and localhost is not included in the database's Computer objects.", ex);
			}
			
			if (testHosts.isEmpty() ) {		// Will probably never see this one
				throw new NoTestHostExcepion("No testHosts found in the database and localhost is not included in the database's Computer objects.");
			}
		}

		// Initialize the cache
		
		this.cache = new Cache();

		
		
		// Get the size of the thread pool
		try {
			String threadPoolSizeStr = System.getenv("TDAQ_TM_TPOOL_SIZE");
			if ( threadPoolSizeStr != null ) {
				int tmp = Integer.parseInt(threadPoolSizeStr);
				if( tmp > 0 ) {
					threadPoolSize = tmp;
				}
			}
		} catch(NumberFormatException ex) {
			ers.Logger.warning(new Issue("Env variable: TDAQ_TM_TPOOL_SIZE  is set, but does not appear to be an interger", ex));
		}
		
		// Create the thread pool
		threadPool = Executors.newFixedThreadPool(threadPoolSize, new TmThreadFactory());
    			
    } // endof Tm()
    
    

    
    /**
     * Run a specific Test for a TestableObject asynchronously, receiving a callback when the Test is done.
     * 
     * @param comp The TestableObject we want to test
     * @param test The specific Test we want to perform
     * @param callback The callback object whose method is to be called once testing is finished @see {@link Callback}
     * @return A Handle to identify the ongoing test allow for stopping it
     * 			or null if there was an error (eg an interruption happened) or the hardware object was disabled
     */
    public TmHandle test(final TestableObject comp, final Test test, final Callback callback)
	throws config.SystemException, config.NotFoundException, config.NotValidException, config.GenericException {
    	try {
	    	this.readLock.lock();
	    	
	    	if ( test == null ) {
	    		ers.Logger.error(new ers.Issue("Need to provide a specific Test!"));
	    		return null;
	    	}
	    	
	    	Tester tester = new Tester(comp, test, callback, this);
	    	
	    	HW_Object hwObj = HW_Object_Helper.cast(comp);
	    	if( hwObj != null && hwObj.get_State() == false ) {		// if this hardware object is disabled ( https://its.cern.ch/jira/browse/ATDAQCCTM-39 )
	    		tester.getCompResult().setComponentResultToHwDisabled();
	    		tester.callUsersCallback();
	    		return null;
	    	} else {
	    		return tester.test();
	    	}
    	} finally {
    		this.readLock.unlock();
    	}
    }
    
  

    /**
     * Same as {@link #test(TestableObject, Callback, Scope, int)} using a default level of {@value #DEFAULT_LEVEL} and a default scope of {@link #DEFAULT_SCOPE}
     * 
     * @param comp The TestableObject we want to test
     * @param callback The callback object whose method is to be called once testing is finished @see {@link Callback}
     * @return A Handle to identify the ongoing test allow for stopping it
     * @throws WrongConfigurationException When there was some error in the configuration of the database
     */
    public TmHandle test(final TestableObject comp, final Callback callback)
	throws NoTestHostExcepion, WrongConfigurationException, config.SystemException, config.NotFoundException, config.NotValidException, config.GenericException {
    	return test(comp, callback, DEFAULT_SCOPE, DEFAULT_LEVEL);
    }
    
    
    /**
     * Same as {@link #test(TestableObject, Callback, Scope, int)} using a default level of {@value #DEFAULT_LEVEL}
     * 
     * @param comp The TestableObject we want to test
     * @param callback The callback object whose method is to be called once testing is finished @see {@link Callback}
     * @param scope The desired scope of the testing (any, precondition, diagnostics, functional)
     * @return A Handle to identify the ongoing test and be able to stop it
     * @throws WrongConfigurationException  When there was some error in the configuration of the database
     */
    public TmHandle test(final TestableObject comp, final Callback callback, final Scope scope)
	throws NoTestHostExcepion, WrongConfigurationException, config.SystemException, config.NotFoundException, config.NotValidException, config.GenericException {
	return test(comp, callback, scope, DEFAULT_LEVEL);
    }
    
    
    /**
     * Test a TestableObject asynchronously, receiving a callback when the Test is done.
     * 
     * @param comp The TestableObject we want to test
     * @param callback The callback object whose method is to be called once testing is finished @see {@link Callback}
     * @param scope The desired scope of the testing (any, precondition, diagnostics, functional)
     * @param level	An int indicating the level of testing (selection of tests out of all available for the component)
     * @return A Handle to identify the ongoing test and be able to stop it 
     * 			or null if there was an error (eg an interruption happened) or the hardware object was disabled 
     * @throws WrongConfigurationException When there was some error in the configuration of the database
     */
    public TmHandle test(final TestableObject comp, final Callback callback, final Scope scope, final int level) 
	throws NoTestHostExcepion, WrongConfigurationException, config.SystemException, config.NotFoundException, config.NotValidException, config.GenericException, WrongConfigurationException {
    	try {    	
	    	this.readLock.lock();
	
	    	Map<String, Map<String, Test>> testsMap = getTestsForTestableObjects( Arrays.asList(new TestableObject[]{comp}), scope, level);
	    	Map<String, Test> tests4Comp = testsMap.get(comp.UID());
	    	
	    	Tester tester = new Tester(comp, tests4Comp, callback, this);
	    	
	    	HW_Object hwObj = HW_Object_Helper.cast(comp);
	    	if( hwObj != null && hwObj.get_State() == false ) {		// if this hardware object is disabled ( https://its.cern.ch/jira/browse/ATDAQCCTM-39 )
	    		tester.getCompResult().setComponentResultToHwDisabled();
	    		tester.callUsersCallback();
	    		return null;
	    	} else {
	    		return tester.test();
	    	}
    	} finally {
	    	this.readLock.unlock();
    	}
    }
    
    
    
    /**
     * Run a specific Test for an application asynchronously, receiving a callback when the Test is done.
     * 
     * @param app The BaseApplication of the Application we want to test
     * @param test The specific Test we want to perform
     * @param callback The callback object whose method is to be called once the Test is finished @see {@link Callback}
     * @return A Handle to identify the ongoing test and be able to stop it
     */
    public TmHandle test(final BaseApplication app, final Test test, final Callback callback) {
    	try {
	    	this.readLock.lock();
	    	if ( test == null ) {
	    		ers.Logger.error(new ers.Issue("Need to provide a specific Test!"));
	    		return null;
	    	}
	    	
	    	Tester tester = new Tester(app, test, callback, this);
	    	return tester.test();
    	} finally {
    		this.readLock.unlock();
    	}
    }
    
    
    
    /**
     * Same as {@link #test(BaseApplication, Callback, Scope, int)} using a default level of {@value #DEFAULT_LEVEL} and a default scope of {@link #DEFAULT_SCOPE}
     * 
     * @param app The BaseApplication of the Application we want to test
     * @param callback The callback object whose method is to be called once testing is finished @see {@link Callback}
     * @return A Handle to identify the ongoing test and be able to stop it
     * @throws WrongConfigurationException  When there was some error in the configuration of the database
     */
    public TmHandle test(final BaseApplication app, final Callback callback)
	throws WrongConfigurationException, config.SystemException, config.NotFoundException, config.NotValidException, config.GenericException {
    	return test(app, callback, DEFAULT_SCOPE, DEFAULT_LEVEL);
    }
    
    
    /**
     * Same as {@link #test(BaseApplication, Callback, Scope, int)} using a default level of {@value #DEFAULT_LEVEL}
     * 
     * @param app The BaseApplication of the Application we want to test
     * @param callback The callback object whose method is to be called once testing is finished @see {@link Callback}
     * @param scope The desired scope of the testing (any, precondition, diagnostics, functional)
     * @return A Handle to identify the ongoing test and be able to stop it
     * @throws WrongConfigurationException When there was some error in the configuration of the database 
     */
    public TmHandle test(final BaseApplication app, final Callback callback, final Scope scope) 
  	throws WrongConfigurationException, config.SystemException, config.NotFoundException, config.NotValidException, config.GenericException {
  	return test(app, callback, scope, DEFAULT_LEVEL);
    } 
    
    
    /**
     * Test an application asynchronously, receiving a callback when testing is done.
     * 
     * @param app the BaseApplication of the application we want to test  
     * @param callback The callback object whose method is to be called once testing is finished @see {@link Callback}
     * @param scope The desired scope of the testing (any, precondition, diagnostics, functional)
     * @param level An int indicating the level of testing (selection of tests out of all available for the component)
     * @return A Handle to identify the ongoing test and be able to stop it
     * @throws WrongConfigurationException When there was some error in the configuration of the database
     */
    public TmHandle test(final BaseApplication app, final jTestManager.Callback callback, final Scope scope, final int level)
	throws WrongConfigurationException, config.SystemException, config.NotFoundException, config.NotValidException, config.GenericException {
    	try {
	    	this.readLock.lock();
	    	
	    	Map<String, Map<String, Test>> testsMap = getTestsForApps( Arrays.asList(new BaseApplication[]{app}), scope, level);
	    	Map<String, Test> tests4App = testsMap.get(app.UID());
	    	
	    	Tester tester = new Tester(app, tests4App, callback, this);
	    	return tester.test();
    	} finally {
    		this.readLock.unlock();
    	}
    }
    
    
	
	/**
	 * Gets the Tests for the given TestableObjects taking into account the scope and level
	 * 
	 * @param testableObjectsList A List of TestableObjects to retrieve the corresponding Tests
	 * @param scope The desired scope of the testing (any, precondition, diagnostics, functional)
	 * @param level	An int indicating the level of testing (selection of tests out of all available for the component)
	 * @return A Map with keys the IDS of the given TestableObjects and values another map with the corresponding, which maps
	 * 			TestIDs to the Test Objects themselves 
	 */
	public Map<String, Map<String, Test>> getTestsForTestableObjects(final List< ? extends TestableObject> testableObjectsList, final Scope scope, final int level)
	throws config.SystemException, config.NotFoundException, config.NotValidException, config.GenericException {
		try {
			final Map<String, Map<String, Test>> theMap = new HashMap<>();


			this.readLock.lock();
		// loop over given testables
			for( final TestableObject testableObj: testableObjectsList ) {
				
				boolean overrideTest4class = false;
				final HashMap<String, Test> testsForComp = new HashMap<>();
				
				// Loop over all tests and check for Tests4Object first
				for( final Test test : this.allTestsSet ) {
					//filter scope and level
					if( test.get_Complexity() > level
						|| (	! scope.equals(Scope.Any)
								&& ! existsInArray(test.get_Scope(), Scope.Any)
								&& ! existsInArray(test.get_Scope(), scope) ) ) {
						continue;
					}
					
					// Loop over all tests and check for Tests4Object first
					final Test4Object testForObject = Test4Object_Helper.cast(test);
					
					if (testForObject != null && testForObject.get_Interactive() == false) {
						for ( final TestableObject candidateTestableObj: testForObject.get_Objects()) {
							if ( candidateTestableObj.UID().equals(testableObj.UID()) ) {
								testsForComp.put(test.UID(), test);
								if (testForObject.get_OverrideTest4Class() == true) {
								// If this attribute set is 'true', Test4Object overrides Test4Class (this is default behavior), i.e. no Test4Class are done
									overrideTest4class = true;
								}
								break;
							}
						}
					}
				} // endof Loop over all tests and check for Tests4Object first
				
				
				
				if(overrideTest4class == false) {		// If not overriden, will also check for test4Class tests
					
					TreeSet <String> classes = this.superClassesMap.get(testableObj.class_name());
					
					for ( final Test test : this.allTestsSet ) {
						//filter scope and level
						if( test.get_Complexity() > level
							|| ( ! scope.equals(Scope.Any) && ! existsInArray(test.get_Scope(), Scope.Any) && ! existsInArray(test.get_Scope(), scope) ) ) {
							continue;
						}
						
						// Cast to special tests class tests
						final Test4CORBAServant testForCORBAServant = Test4CORBAServant_Helper.cast(test);
						final Test4Class test4Class = Test4Class_Helper.cast(test);
						if (testForCORBAServant != null) {
							for (final String className : testForCORBAServant.get_ClassName()) {
								if (classes.contains(className)) {
									testsForComp.put(test.UID(), test);
									break;
								}
							}
						} else if (test4Class != null && test4Class.get_Interactive() == false) {
							if ( classes.contains(test4Class.get_ClassName()) ) {
								testsForComp.put(test.UID(), test);
							}
						}
					}
				} // endof if(overrideTest4class == false)
				
				
				theMap.put(testableObj.UID(), testsForComp);
				
			}// endof loop over given testables
			
			return theMap;
		} finally {
			this.readLock.unlock();
		}
	}
	
    
    
    
    /**
     * Gets the Tests for the given BaseApplicationss taking into account the scope and level
     * 
	 * @param appList A List of BaseApplicationss to retrieve the corresponding Tests
	 * @param scope The desired scope of the testing (any, precondition, diagnostics, functional)
	 * @param level	An int indicating the level of testing (selection of tests out of all available for the component)
	 * @return A Map with keys the IDS of the given BaseApplicationss and values another map with the corresponding, which maps
	 * 			TestIDs to the Test Objects themselves 
     */
	public Map<String, Map<String, Test>> getTestsForApps(final List< ? extends BaseApplication> appList, final Scope scope, final int level) 
	throws config.SystemException, config.NotFoundException, config.NotValidException, config.GenericException {
		try {
			this.readLock.lock();
			final Map<String, Map<String, Test>> theMap = new HashMap<>();

			// loop over given apps
			for( final BaseApplication comp : appList ) {
	
				boolean overrideTest4class = false;
				final HashMap<String, Test> testsForComp = new HashMap<>();
				
				// Loop over all tests and check for Tests4Object first
				for( final Test test : this.allTestsSet ) {
					//filter scope and level
					if( test.get_Complexity() > level
						|| (	! scope.equals(Scope.Any)
								&& ! existsInArray(test.get_Scope(), Scope.Any)
								&& ! existsInArray(test.get_Scope(), scope) ) ) {
						continue;
					}
					
					// Check if it is Test4Object
					final Test4Object testForObject = Test4Object_Helper.cast(test);
					
					if (testForObject != null && testForObject.get_Interactive() == false) {
						for ( final TestableObject candidateTestableObj: testForObject.get_Objects()) {
							if ( candidateTestableObj.UID().equals(comp.UID()) ) {
								testsForComp.put(test.UID(), test);
								if (testForObject.get_OverrideTest4Class() == true) {
								// If this attribute set is 'true', Test4Object overrides Test4Class (this is default behavior), i.e. no Test4Class are done
									overrideTest4class = true;
								}
								break;
							}
						}
					}
				} // endof Loop over all tests and check for Tests4Object first
				
				
				
				if(overrideTest4class == false) {		// If not overridden, will also check for test4Class tests
					
					TreeSet <String> classes = this.superClassesMap.get(comp.class_name());
					
					for ( final Test test : this.allTestsSet ) {
						//filter scope and level
						if( test.get_Complexity() > level
							|| ( ! scope.equals(Scope.Any) && ! existsInArray(test.get_Scope(), Scope.Any) && ! existsInArray(test.get_Scope(), scope) ) ) {
							continue;
						}
						
						// Cast to special tests class tests
						final Test4CORBAServant testForCORBAServant = Test4CORBAServant_Helper.cast(test);
						final Test4Class test4Class = Test4Class_Helper.cast(test);
						if (testForCORBAServant != null) {
							for (final String className : testForCORBAServant.get_ClassName()) {
								if (classes.contains(className)) {
									testsForComp.put(test.UID(), test);
									break;
								}
							}
						} else if (test4Class != null && test4Class.get_Interactive() == false) {
							if ( classes.contains(test4Class.get_ClassName()) ) {
								testsForComp.put(test.UID(), test);
							}
						}
					}
				} // endof if(overrideTest4class == false)
				
				theMap.put(comp.UID(), testsForComp);
				
			} // endof loop over given apps
			return theMap;
		} finally {
			this.readLock.unlock();
		}
	}

	
	
	/** 
	 * Reloads the database configuration (to be used after changes in the database that are related to the testing)
	 * <p>
	 * <b>PRECONDITION:</b> the user that calls reload must have properly reloaded the database (configuration object
	 *
	 * @throws NoTestHostExcepion when there was some problem in getting TestHosts
	 * @throws InterruptedException An interruption happened during the reload
     * @throws WrongConfigurationException when there was some problem wit the configuration
	 */
	public void reload() throws NoTestHostExcepion, InterruptedException, WrongConfigurationException,
		config.SystemException, config.NotFoundException, config.NotValidException, config.GenericException {
    	try {
	    	this.writeLock.lock();
	    	
	    	/*
	    	 * Kill the thread pool
	    	 */
	    	this.threadPool.shutdownNow();
	    	
			while ( this.threadPool.awaitTermination(1, TimeUnit.SECONDS) == false) {
				ers.Logger.info("Reaload: Waiting for thread pool tasks to terminate");
			}
			
	    	
	    	
	    	
	    	/*
	    	 * Change the configuration info of the TestManager
	    	 */
	    	
			try {
		    	this.partition = dal.Algorithms.get_partition(config, this.partitionName );
	
				BaseApplication[] appArray = this.partition.get_all_applications(null, null, null);
				this.appMap.clear();
				for(BaseApplication app : appArray) {
					this.appMap.put(app.UID(), app);
				}
			} catch (config.SystemException | config.NotFoundException ex) {
	    		throw new WrongConfigurationException(ex);
	    	}
			
			// Get all the test
			
			this.allTestsSet.clear();
			addTests(allTestsSet, this.partition);	
			addTests(allTestsSet, this.partition.get_OnlineInfrastructure());
			
			
			// Get all the superclasses
			
			this.superClassesMap = this.config.superclasses(); // Get superclasses of each class
			for ( Entry<String, TreeSet<String>> e : this.superClassesMap.entrySet()) {
				e.getValue().add(e.getKey());			// add the class itself to the set of its superclasses
			}
			
			
			// Add the TestHosts
			
			ComputerSet computerSet = this.partition.get_OnlineInfrastructure().get_TestHosts();
			testHosts = getAllComputers(computerSet);
			
			if (testHosts.isEmpty()) {		// If there were none TestHosts, add the default Host
				Computer pc = this.partition.get_DefaultHost();
				if( pc != null ) {
					testHosts.add(pc);
				}
			}
			
			if (testHosts.isEmpty()) {		// If there was no DefaultHost for the partition, add the default Host
				try {
					java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
					ConfigObject localhostConfigObj = this.config.get_object("Computer", localMachine.getCanonicalHostName());
					
					Computer localhostComputer = Computer_Helper.get(this.config, localhostConfigObj);
					if( localhostComputer != null) {
						testHosts.add(localhostComputer);
					}
				} catch (UnknownHostException ex) {
					throw new NoTestHostExcepion("No testHosts found in the database and the IP address of localhost could not be determined.", ex); 
				} catch (NotFoundException ex) {
					throw new NoTestHostExcepion("No testHosts found in the database and localhost is not included in the database's Computer objects.", ex);
				}
				
				if (testHosts.isEmpty() ) {		// Will probably never see this one
					throw new NoTestHostExcepion("No testHosts found in the database and localhost is not included in the database's Computer objects.");
				}
			}
			
			// re-initialize the "cache" by creating a new object and keeping no reference of the old one/
			this.cache = new Cache();

			
			// Create the thread pool
			threadPool = Executors.newFixedThreadPool(threadPoolSize, new TmThreadFactory());
			ers.Logger.log("Using a threadpool with size " + this.threadPoolSize );
    	} finally {
    		this.writeLock.unlock();
    	}
    }
    
    
	

    
    
    /**
     * Resets the validity of all the tests' results for a TestableObject.
     * 
     * @param comp The TestableObject for which the reset is performed
     */
    public void reset(final TestableObject comp)
	throws config.SystemException, config.NotFoundException, config.NotValidException, config.GenericException {
    	 Map<String, Test> allTestsForComp = getTestsForTestableObjects(Arrays.asList(new TestableObject[]{comp}), Scope.Any, MAX_LEVEL).get(comp.UID());
    	for( Test test : allTestsForComp.values() ) {
        	this.cache.reset(comp.class_name() + "@" + comp.UID(), test.UID());	// see Tester constructor for compCacheUID
    	}
    }
    
    
    /**
     * Resets the validity of all the tests' results for an Application.
     * 
     * @param comp The {@link BaseApplication} for which the reset is performed
     */
    public void reset(final BaseApplication comp)
	throws config.SystemException, config.NotFoundException, config.NotValidException, config.GenericException {
	   	Map<String, Test> allTestsForComp = getTestsForApps(Arrays.asList(new BaseApplication[]{comp}), Scope.Any, MAX_LEVEL).get(comp.UID());
	   	for( Test test : allTestsForComp.values() ) {
	       	this.cache.reset(comp.class_name() + "@" + comp.UID(), test.UID());	// // see Tester constructor for compCacheUID
	   	}
    }
    
    
    /**
     * Shuts-down the Test Manager and waits for the internal Threads to finish (max waiting 3 seconds)
     */
    public void shutdown() {
    	try {
    		this.readLock.lock();
    		this.threadPool.shutdown();
	    	while (true) {
		    	try {
					this.threadPool.awaitTermination(3, TimeUnit.SECONDS);
					break;
				} catch (InterruptedException ignored) { }
	    	}
    	} finally {
    		this.readLock.unlock();
    	}
    }

    
    
    /**
     * Returns a List with all the Tests of the loaded configuration.
     * The actual List used by The TestManager itself is return, so no changes should be made in that object. //TODO return a copy?
     * The user must make a copy of the List if there is need to make changes. (No reloading should be performed while copying)
     * 
     * @return The aforementioned List
     */
    public List<Test> getAllTestsSet() {
    	try {
    		this.readLock.lock();
    		return this.allTestsSet;
    	} finally {
    		this.readLock.unlock();
    	}
    }
    
    
    /**
     * Verbose means that argument "-v" is added to every executable that the TestManager runs.
     * By convention, this should be recognized by the executables as "activation of verbose mode"
     * 
     * @return true if verbosity is activated 
     */
    public boolean getIsVerbose() {
    	return this.isVerbose.get();
    }

    
    
    /**
     * @see {@link Tm#setDiscardOutput(boolean)}
     * @return true is outputs of the Tests are discarded, false otherwise.
     */
    public boolean getDiscardOutput() {
    	return this.discardOutput.get();
    }
	
    
	/**
	 * @return The name of the partition
	 */
	public String getPartitionName() {
    	try {
    		this.readLock.lock();
    		return this.partitionName;
    	} finally {
    		this.readLock.unlock();
    	}
	}
	
	
	/**
	 * The actual List used by The TestManager itself is return, so no changes should be made in that object. //TODO return a copy?
     * The user must make a copy of the List if there is need to make changes. (No reloading should be performed while copying)
     * 
	 * @return A List with all the TestHosts
	 */
	public List<Computer> getTestHosts() {
    	try {
    		this.readLock.lock();
    		return this.testHosts;
    	} finally {
    		this.readLock.unlock();
    	}
	}
	
	
	/**
	 * The size of the used thread pool can be changed befoore launch by setting "TDAQ_TM_TPOOL_SIZE". By default the size is 16
	 * 
	 * @return The size of the internal ThreadPool used by the TestManager
	 */
	public int getThreadPoolSize() {
		return this.threadPoolSize;
	}

    
    
    /**
     * @param debugLevel Currently ignored
     * @param verbosityLevel true sets on the verbose mode @see {@link Tm#getIsVerbose()}, false unsets it
     */
    public void setVerbosity(final boolean isVerbose) {
    	this.isVerbose.set(isVerbose);
    }
    
    
    
    /**
     * Choose whether to discard the output of tests, ie null logs for the launched processes
     * 
     * @param discard If true, output will be discarded. 
     */
    public void setDiscardOutput(boolean discard) {
    	this.discardOutput.set(discard);
    }
	
    
    
	/*
	 * 
	 * 		Non public part - Methods used only by the TM itself
	 *
	 */
	
	
    /**
     * 
     * @return the Configuration object under use
     */
    Configuration getConfig() {
    	try {
    		this.readLock.lock();
    		return this.config;
    	} finally {
    		this.readLock.unlock();
    	}
    }
	
    /**
     * 
     * @return the Process Manager that is used by the Test Manager
     */
	PmgClient getPmg() {
		return this.pmg;
	}
	
	/**
	 * 
	 * @return the Partition object used by the TestManager
	 */
	Partition getPartition() {
    	try {
    		this.readLock.lock();
    		return this.partition;
    	} finally {
    		this.readLock.unlock();
    	}
	}
	
	
	
	/**
	 * 
	 * @return a random host from the all the TestHosts in the partition
	 */
	Computer getTestHost() {		
    	try {
    		this.readLock.lock();
    		return this.testHosts.get(ThreadLocalRandom.current().nextInt(0, this.testHosts.size()));
    	} finally {
    		this.readLock.unlock();
    	}
	}
	
	
	/**
	 * Wraps the @link {@link Cache#initializeResult(String, SpecificTestResult)}.
	 * This methods has to be called t the beginning of the Test as soon as the SpecificTestResult is ready.
	 * 
	 * @param compCacheUID  A UID of the component to which the Test belongs to. Used to construct the cacheUID
	 * @param specificTestResult The SpecificTestResult object were the result of the Test will be store once the Test is done
	 * @return A CachedSpecificTestResult which should be kept and used to call the {@link CachedSpecificTestResult#commit()}
	 *			when the Test is done. Null is returned if the given Test cannot be cached 
	 */
	Cache.CachedSpecificTestResult initializeToCache(final String compCacheUID, final SpecificTestResult specificTestResult) {
		return this.cache.initializeResult(compCacheUID, specificTestResult);
	}
	
	
	/**
	 * Checks for cached versions of the given Test for the given component
	 * 
	 * @param compCacheUID UID of the component to which the Test belongs to. Note that this is not the UID of the comp! @see {@link Tester#getCompCacheUID()}
	 * @param testID ID of the Test for which the cache will be checked for
	 * @return the cached result for the given Test of the given component or null if the result is not cached or is invalid
	 */
	SpecificTestResult getCachedResult(final String compCacheUID, final String testID) {
		return this.cache.getCachedResult(compCacheUID, testID);
	}

	
	/**
	 * 
	 * @return A map with all the BaseApplications, mapping the appID to BaseApplication objects
	 */
	Map<String, BaseApplication> getAppMap() {
		return this.appMap;
	}
	
	
	/**
	 * @return the ThreadPool used by TM
	 */
	ExecutorService getThreadPool() {
		return this.threadPool;
	}
	
	
	/**
	 * @return The read Lock used by the TM
	 */
	Lock getReadLock() {
		return this.readLock;
	}
	
	
	/**
	 * Given a ComputerSet from the partition, returns a List with all the Computers
	 * 
	 * @param computerSet
	 * @return The List with all the computers of the partition
	 */
	private List<Computer> getAllComputers( ComputerSet computerSet )
	throws config.SystemException, config.NotFoundException, config.NotValidException, config.GenericException {
		final List<Computer> computerList = new ArrayList<>();
		
		if( computerSet == null ) {
			return computerList;
		}
		final ComputerBase[] computerBaseArray = computerSet.get_Contains();
		
		for (ComputerBase computerBase : computerBaseArray) {
			
			final Computer pc = Computer_Helper.cast(computerBase);
			if (pc != null && pc.get_State() == true) {
				computerList.add(pc);
			}
			final ComputerSet pcSet = ComputerSet_Helper.cast(computerBase);
			if ( pcSet != null ) {
				computerList.addAll(getAllComputers(pcSet));
			}
		}
		
		return computerList;
	}
	
	
	/**
	 * Check whether the Scope KEY is included in the String[] STRS. The comparison uses equalsIgnoreCase
	 * 
	 * @param strs
	 * @param key
	 * @return
	 */
	private boolean existsInArray(String [] strs, Scope key) {
		for( final String s : strs ) {
			if( s.equals(key.toString())) {
				return true;
			}
		}
		return false;
	}
	
    
	/**
	 * Adds all the Tests of the given Partition (and its Segments) to the given List.
	 * 
	 * @param tests The List in which the Tests will be added.
	 * @param part The Partition object
	 */
	private void addTests(final List<Test> tests, final Partition part)
	throws config.SystemException, config.NotFoundException, config.NotValidException, config.GenericException {
		
		for (final TestRepository i : part.get_TestRepositories()) {
			final TestSet ts =  TestSet_Helper.cast(i);
			if (ts != null) {
				tests.addAll(Arrays.asList(ts.get_tests()));
			}
		}

		for (final Segment s : part.get_Segments()) {
			addTests(tests, s);
		}
	}


	/**
	 *  Adds all the Tests of the given Segment (and its Segments) to the given List.
	 * 
	 * @param tests The List in which the Tests will be added.
	 * @param seg The Segment
	 */
	private void addTests(final List<Test> tests, final Segment seg)
	throws config.SystemException, config.NotFoundException, config.NotValidException, config.GenericException {
		
		for (final TestRepository i : seg.get_TestRepositories()) {
			final TestSet ts = TestSet_Helper.cast(i);
			if (ts != null) {
				tests.addAll(Arrays.asList(ts.get_tests()));
			}
		}

		for (final Segment s : seg.get_Segments()) {
			addTests(tests, s);
		}
	}
	

	
}



