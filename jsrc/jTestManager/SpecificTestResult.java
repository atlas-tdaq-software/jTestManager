package jTestManager;

import TM.Test;

/**
 * Describes the outcome of a single ExecutableTest (Test), which is related (1-to-N relationship) with 1 or more Executables.
 * The SpecificTestResult is determined from the individual Executables' results by following a similar logic to the one here
 *  {@link https://twiki.cern.ch/twiki/bin/viewauth/Atlas/DaqHltTestManager#Test_Result_of_a_component_and_h}
 *
 */
public class SpecificTestResult {

	private final String testID;
	private final int testValidity;
    private String stdout;
    private String stderr;
    private int testReturnCode;
    private boolean hadUndef = false;
	
	
	SpecificTestResult(final Test test) throws config.ConfigException{
		this.testID = test.UID();
		this.testValidity = test.get_Validity();
		this.stderr = "";
		this.stdout = "";
		this.testReturnCode = TestResult.TM_UNDEF.getInt();
	}
	
	
	/**
	 * @return the return code of Test. In case of multiple executable in a single Test this value is calculated with similar 
	 * logic to the one here {@link https://twiki.cern.ch/twiki/bin/viewauth/Atlas/DaqHltTestManager#Test_Result_of_a_component_and_h} 
	 */
	public synchronized int getTestReturnCode() {
		return this.testReturnCode;
	}

	
	/**
	 * @return The ID of the Test with this result
	 */
	public String getTestID() {
		return this.testID;
	}
	
	
	/**
	 * @return The validity of the Test in seconds
	 */
	public int getTestValidity() {
		return this.testValidity;
	}

	
	/**
	 * @return The output stream of the Test
	 */
	public synchronized String getStdout() {
		return this.stdout;
	}
	

	/**
	 * @return The error stream of the Test
	 */
	public synchronized String getStderr() {
		return this.stderr;
	}
	
	
	/*
	 * 
	 * Non-public part
	 * 
	 */


	/**
	 * Updates the return code of the Test given the return code of an executable.
	 * If the Test had more than one executables to be run then this value is calculated with similar 
	 * logic to the one here {@link https://twiki.cern.ch/twiki/bin/viewauth/Atlas/DaqHltTestManager#Test_Result_of_a_component_and_h} 
	 * 
	 * @param returnCode the return code of some Executable for this Test
	 */
	synchronized void updateWithExecReturnCode(final int returnCode) {
		
		// If it was TM_UNRESOLVED before and the current executable's result is also TM_UNRESOLVED, do nothing.
		// This is done in order to preserve the UNRESOLVED status for the Test and handle it accordingly if RepeatIfUnresolved is true
		if ( returnCode == TestResult.TM_UNRESOLVED.getInt() ) {
			if ( this.testReturnCode == TestResult.TM_UNRESOLVED.getInt() ) {
				return;
			} else if ( this.testReturnCode == TestResult.TM_UNDEF.getInt() )  {
				this.testReturnCode = TestResult.TM_UNRESOLVED.getInt();
				return;
			}
		}
		
		/*
		 * Handle some cases where test's result = UNRESOLVED
		 */
		if ( this.testReturnCode == TestResult.TM_UNRESOLVED.getInt() ) {
			// the return code of the executable can be: UNDEF, (UNTESTED), PASS or (FAIL)
			// (the ones in the parenthesis are handled correctly later)
			if ( returnCode == TestResult.TM_UNDEF.getInt() || returnCode == TestResult.TM_PASS.getInt() ) {
				this.testReturnCode = TestResult.TM_UNTESTED.getInt();
				return;
			}
		}
		
		
		/*
		 * if test's result was already FAIL, it will always stay that way
		 * if the specific executable result was fail, then the test result is set to fail
		 * 
		 * For what 'fail' is as far as specific executables are concerned see isFail() method
		 */
		if ( this.testReturnCode == TestResult.TM_FAIL.getInt() || CompResult.isFail(returnCode) ) {
			this.testReturnCode = TestResult.TM_FAIL.getInt();
			return;
		}
		
		/*
		 * At this point the test's result can be: UNDEF, UNTESTED, UNRESOLVED** or PASS
		 * and the executable result can be: UNDEF, UNTESTED, UNRESOLVED or PASS
		 * (but not all their combinations)
		 */
		
		if ( returnCode == TestResult.TM_UNTESTED.getInt() || returnCode == TestResult.TM_UNRESOLVED.getInt()
				|| this.testReturnCode == TestResult.TM_UNTESTED.getInt() ) {
			this.testReturnCode = TestResult.TM_UNTESTED.getInt();
			return;
		}
		
		/*
		 * At this point the test's result can be: UNDEF  or PASS
		 * and the executable can be: UNDEF or PASS
		 */
		
		if ( this.testReturnCode == TestResult.TM_PASS.getInt() ) {
			if ( returnCode == TestResult.TM_UNDEF.getInt() ) {
				this.hadUndef = true;
				this.testReturnCode = TestResult.TM_UNDEF.getInt();
				
			} else if (returnCode == TestResult.TM_UNRESOLVED.getInt()) {
				this.testReturnCode = TestResult.TM_UNTESTED.getInt();
			}
			// else the executable is PASS, so we do not need to modify the test's result, i.e. leave it TM_PASS
			return;
		}
		
		/*
		 * At this point the test's result is: UNDEF
		 * and the executable can be: UNDEF or PASS
		 */
		
		if ( returnCode == TestResult.TM_PASS.getInt() ) {
			if ( ! this.hadUndef ) {
				this.testReturnCode = TestResult.TM_PASS.getInt();
			}
			// else leave it as is, i.e. componentResult == UNDEF
			return;
		}
		
		/*
		 * At this point the test's result is: UNDEF
		 * and the executable is: UNDEF
		 */

		if( this.testReturnCode == TestResult.TM_UNDEF.getInt() && returnCode == TestResult.TM_UNDEF.getInt() )  {
			this.hadUndef = true;
			// and the test's result stays as is, i.e. UNDEF
			return;
		}
		
		// this should never be reached unless there was a mistake in the logic above
		ers.Logger.error(new ers.Issue("there is some error in the updateWithReturnCode method. Missed cases. Check the code"));
		

	}
	
	
	/**
	 * Same functionality @see SpecificTestResult#updateWithExecReturnCode(int) with TestResult type of input
	 * 
	 * @param returnCode the TestResult of some Executable for this Test
	 */
	synchronized void updateWithExecReturnCode(final TestResult returnCode) {
		this.updateWithExecReturnCode(returnCode.getInt());
	}


	/**
	 * Appends the given String to the stdout of this Test separating with newline from the previous values
	 * 
	 * @param stdout
	 */
	synchronized void updateStdout(final String stdout) {
		if (stdout.isEmpty()) {
			return;
		}
		if (this.stdout.isEmpty()) {
			this.stdout = stdout;
		} else {
			this.stdout = this.stdout + "\n" + stdout;
		}
	}
	

	/**
	 * Appends the given String to the stderror of this Test separating with newline from the previous values
	 * 
	 * @param stderr
	 */
	synchronized void updateStderr(final String stderr) {
		if (stderr.isEmpty()) {
			return;
		}
		if (this.stderr.isEmpty()) {
			this.stderr = stderr;
		} else{
			this.stderr = this.stderr + "\n" + stderr;
		}
	}	
	
	

	
}
