package jTestManager;

import dal.BaseApplication;

/**
 * 
 * The Action corresponding to the failure TestUponFailure in case the component to be tested is an application
 *
 */
public class TestApplicationAction extends Action {

	private BaseApplication app;
	
	public TestApplicationAction(BaseApplication appConf, String diagnosis) {
		super(diagnosis);
		this.app = appConf;
	}
	
	/**
	 * 
	 * @return The application to be tested
	 */
	public BaseApplication getApplication() {
		return this.app;
	}

}
