package jTestManager;

import java.util.Arrays;
import java.util.List;

import TM.Executable;
import dal.Computer;
import dal.ComputerProgram;
import dal.Computer_Helper;

/**
 * Class that contains a basic description of an executable and is used in the ExecAction
 */
public class Exec {
	
	final public Computer host;
	
	final public String cmdLinePars;
	final public int timeout;
	final public int initTimeout;
	final public ComputerProgram program;
	final List<Executable> initDep;
        

	/**
	 * 
	 * @param executable
	 * @param tester
	 * @param tm
	 * @throws WrongConfigurationException
	 * @throws InterruptedException 
	 */
	Exec(final int timeout, final Executable executable, final Tester tester, final Tm tm)
	throws config.ConfigException, WrongConfigurationException, InterruptedException {
		
		final String host = executable.get_Host();
		
		if ( host.isEmpty() ) {
			this.host = tm.getTestHost();
		} else {
			this.host = Computer_Helper.get(tm.getConfig(), tester.substituteParam(host));
		}
		

		final String params = executable.get_Executes().get_DefaultParameters() + " " + executable.get_Parameters();
		this.cmdLinePars = tester.substituteParam(params);		// throws WrongConfigurationException

		
		this.timeout = timeout;
		this.initTimeout = executable.get_InitTimeout();
		this.program = executable.get_Executes();
		this.initDep = Arrays.asList( executable.get_InitDependsOn() );
		
	}

}
