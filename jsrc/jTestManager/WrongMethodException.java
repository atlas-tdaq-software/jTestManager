package jTestManager;


/**
 * Thrown when the wrong method of DependenciesSolver is used in order to solve the dependencies,
 * eg Dependencies solver was made for Test, but the method for Executables is called.
 *
 */
public class WrongMethodException extends RuntimeException {

	private static final long serialVersionUID = 3438035982426141297L;

	public WrongMethodException(String message) {
		super(message);
	}
}
