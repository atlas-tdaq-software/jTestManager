package jTestManager;

/**
 * 
 * Generic Action corresponding to a general Failure. More specific Action should inherit from this one.
 *
 */
public class Action {
	
	private String diagnosis;

	public Action(final String diagnosis) {
		this.diagnosis = diagnosis;
	}
	
	/**
	 * 
	 * @return A String with the diagnosis for this Action
	 */
	public String getDiagnosis() {
		return this.diagnosis;
	}

}
