package jTestManager;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

import org.jgrapht.alg.CycleDetector;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.traverse.TopologicalOrderIterator;

import TM.Executable;
import TM.Test;

/**
 * A class used to solve dependencies in a Set of Test or an array of Executables
 *
 */
class DependenciesSolver {
	
	private DefaultDirectedGraph<Test, DefaultEdge> dependenciesGraphTest;
	private DefaultDirectedGraph<Executable, DefaultEdge> dependenciesGraphExec;
	
	/**
	 * Builds a dependency graph based on the given Set of Tests and also checks whether there are any cycles.
	 * A -> B (B depends on A, i.e. A must be done before B)
	 * 
	 * @param tests	A HashMap<String, Test> of TestUID and Tests, for which we want to find the dependencies
	 * @throws WrongConfigurationException If there is a dependencies cycle between the given Tests, i.e. A->B->A.
	 *  This indicates an error in the configuration.
	 */
	DependenciesSolver(final Map<String, Test> tests)
		throws WrongConfigurationException, config.GenericException, config.NotFoundException, config.SystemException, config.NotValidException {
		this.dependenciesGraphExec = null;
		this.dependenciesGraphTest = new DefaultDirectedGraph<Test, DefaultEdge>(DefaultEdge.class);
		
		for( Entry<String, Test> entry : tests.entrySet() ) {				// Adds Tests as vertices
			this.dependenciesGraphTest.addVertex(entry.getValue());
		}
		
		for( Entry<String, Test> entry : tests.entrySet() ) {				// Add the edges corresponding to the dependencies between tests
			try {
				Test test = entry.getValue();
				for( Test reqTest : test.get_ExecDependsOnFailure() ) {
					if( ! tests.containsKey(reqTest.UID()) ) {	// Test may need to be ignored because of its has a scope or level
						continue;
					}
					this.dependenciesGraphTest.addEdge(reqTest, test );
				}
				for( Test reqTest : test.get_ExecDependsOnSuccess() ) {
					if( ! tests.containsKey(reqTest.UID()) ) {	// Test may need to be ignored because of its has a scope or level
						continue;
					}
					this.dependenciesGraphTest.addEdge(reqTest, test );
				}
			} catch( IllegalArgumentException ex ) {	// Thrown if source or target vertices are not contained in the graph.
				throw new WrongConfigurationException("Test: " + entry.getKey()
						+ " had dependency on a test that was not included in the given Set" );
			}
		}
		
		CycleDetector<Test, DefaultEdge> cycleDetector = new CycleDetector<>(this.dependenciesGraphTest);
		if( cycleDetector.detectCycles() ) {
			throw new WrongConfigurationException("A dependency cycle was found in the given set of tests.");
		}
		
	}
	
	
	/**
	 * Builds a dependency graph based on the given array of Executables and also checks whether there are any cycles.
	 * A -> B (A depends on B, i.e. B must be done before A)
	 * 
	 * @param execs	An array of the Executables, for which we want to find the dependencies
	 * @throws WrongConfigurationException If there is a dependencies cycle between the given Executables, i.e. A->B->A.
	 *  This indicates an error in the configuration.
	 */
	DependenciesSolver(final Executable[] execs)
	throws WrongConfigurationException, config.GenericException, config.NotFoundException, config.SystemException, config.NotValidException {
		
		this.dependenciesGraphTest = null;
		this.dependenciesGraphExec = new DefaultDirectedGraph<Executable, DefaultEdge>(DefaultEdge.class);
		
		for( Executable exec : execs ) {				// Adds Executable UIDs as vertices
			this.dependenciesGraphExec.addVertex(exec);
		}
		
		for( Executable exec : execs ) {				// Add the edges corresponding to the dependencies between Executables
			for( Executable reqExec : exec.get_InitDependsOn() ) {
				try {
					this.dependenciesGraphExec.addEdge(reqExec, exec);
				} catch( IllegalArgumentException ex ) {	// Thrown if source or target vertices are not contained in the graph.
					throw new WrongConfigurationException("Executable: " + exec.UID()
							+ " had dependency on an executable that was not included in the given Executables" );
				}
			}
		}
		
		CycleDetector<Executable, DefaultEdge> cycleDetector = new CycleDetector<>(this.dependenciesGraphExec);
		if( cycleDetector.detectCycles() ) {
			throw new WrongConfigurationException("A dependency cycle was found in the given Executables.");
		}
	}
	
	
	/**
	 * Gives a solution (topological order) for the Tests of the graph,
	 * i.e. an order in which the Tests can be executed with respect to their dependencies
	 * Each HashSet in the result contains Tests that can be safely executed in parallel.
	 * 
	 * @return an ArrayList indicating the order in which the Sets of Tests should be executed, starting from the beginning of the list,
	 * i.e. all elements in the first Set in the ArrayList have no dependencies and are the first ones that should be executed
	 */
	List<List<Test>> getGroupedTopologicalOrderedTests()
	throws WrongMethodException, config.GenericException, config.NotFoundException, config.SystemException, config.NotValidException {
			// TODO group the Tests that can be executed in parallel, now this only work for the 1st group
		if (this.dependenciesGraphTest == null) {
			throw new WrongMethodException("Called the method for Tests, but the graph seems to have Executables.");
		}
		TopologicalOrderIterator<Test, DefaultEdge> orderIterator = new TopologicalOrderIterator<Test, DefaultEdge>(this.dependenciesGraphTest);
		List<List<Test>> topologicalOrderGrouped = new ArrayList<>();
		
		if (orderIterator.hasNext()) {		// if there is a first one (maybe we have not tests
			topologicalOrderGrouped.add(new ArrayList<Test>());	// manually add the 1st set, which will include all the Tests with no dependencies at all
		}
		
		while (orderIterator.hasNext()) {			
			Test node = orderIterator.next();
            if ( node.get_ExecDependsOnFailure().length == 0 && node.get_ExecDependsOnSuccess().length == 0 ) {		
            	/* If there are N tests with no dependencies at all, those will be in the N first nodes of the topological sort.
            	 * So if this is one of those N nodes, add it in the first set
            	 */
//            	System.out.print( node.UID() + ", ");
            	topologicalOrderGrouped.get(0).add(node);
            	
            } else {
//            	System.out.print(" || "); // separate groups
//                System.out.print( node.UID() + ", ");
            	List<Test> independentObjects = new ArrayList<Test>();
	            independentObjects.add(node);
	            topologicalOrderGrouped.add( independentObjects );
            }
         }
//		System.out.println("");
		
		return topologicalOrderGrouped;
	}
	
	
	/**
	 * Gives a solution (topological order) for the Executables of the graph,
	 * i.e. the order that the Executables should reach RUNNING state
	 * Each HashSet in the result contains the Executables that can be run in parallel.
	 * 
	 * @return an ArrayList indicating the order in which the Executables must be run, starting from the beginning of the list,
	 * i.e. all elements in the first Set in the ArrayList have no dependencies and are the first ones that should be executed
	 */
	List<List<Executable>> getGroupedTopologicalOrderedExecutables() {
		if (this.dependenciesGraphExec == null) {
			throw new WrongMethodException("Called the method for Executables, but the graph seems to have Tests.");
		}
		TopologicalOrderIterator<Executable, DefaultEdge> orderIterator = new TopologicalOrderIterator<Executable, DefaultEdge>(this.dependenciesGraphExec);
		List<List<Executable>> topologicalOrderGrouped = new ArrayList<>();
		
		while (orderIterator.hasNext()) {			// TODO group the Executables that can be executed in parallel
			Executable node = orderIterator.next();
         //   System.out.print( node + ", ");
            List<Executable> independentObjects = new ArrayList<Executable>();
            independentObjects.add(node);
            topologicalOrderGrouped.add( independentObjects );
         }
		//System.out.println("");
		
		return topologicalOrderGrouped;
	}
}
