package jTestManager;


/**
 * Used as a base class for all the Exceptions thrown by the TestManager
 * <p>
 * User code can safely assume that all the exceptions thrown by the TestManager will inherit from this class.
 * However, it is recommended to catch each specific exception separately and act accordingly<br>
 * <p>
 * Maintenance notes: 
 * Every exception thrown by the TestManager must inherit from this class. <br>
 * Not to be used directly in the library code. Use other exception that extend the TestManagerException or write new ones that do.
 *
 */
public class TestManagerException extends ers.Issue {

	private static final long serialVersionUID = -6194091565341880018L;

	public TestManagerException(String string) {
		super(string);
	}

	public TestManagerException(String string, Exception ex) {
		super(string, ex);
	}

	public TestManagerException(Exception ex) {
		super(ex);
	}

}
