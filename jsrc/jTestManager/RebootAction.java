package jTestManager;

import dal.Computer;

/**
 * 
 * The Action corresponding to the failure RebootUponFailure
 *
 */
public class RebootAction extends Action {
	
	private Computer host;

	public RebootAction(Computer pc, String diagnosis) {
		super(diagnosis);
		this.host = pc;
	}
	
	/**
	 * 
	 * @return The host to be rebooted
	 */
	public Computer getHost() {
		return this.host;
	}

}
