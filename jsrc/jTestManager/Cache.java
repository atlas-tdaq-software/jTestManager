package jTestManager;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * The class used for the caching of each Test (when supported). Use only once instance of the cache object.
 * Expected use: check if there is a valid cached result and if not initialize a new one at the beginning of the Test.
 * When the test is done, one should commit the cached result in order to signify that the test is not longer running, i.e. it's finished 
 *
 */
class Cache {
	
	final private ConcurrentMap<String, CachedSpecificTestResult> cachedResults;
	
	
	/**
	 * This class wraps the {@SpecificTestResult} and adds some more info regarding the cache
	 */
	static class CachedSpecificTestResult {
		private long validTill;
		private final SpecificTestResult specificTestResult;
		private boolean isOngoing;
		
		private CachedSpecificTestResult( final SpecificTestResult specificTestResult) {
			this.specificTestResult = specificTestResult;
			this.validTill = System.currentTimeMillis() + specificTestResult.getTestValidity() * 1000;
			this.isOngoing = true;
		}
		
		/**
		 * Method to be called by the user of the cache of the object returned by {@link Cache#initializeResult(String, SpecificTestResult)}
		 */
		void commit() {
			this.isOngoing = false;
		}
	}
	

	
	Cache() {
		this.cachedResults = new ConcurrentHashMap<>(32, 0.9f, 4);
	}
	
	
	/**
	 * Prepares the cache for the given @SpecificTestResult .
	 * 
	 * @param compCacheUID A UID of the component to which the Test belongs to. Used to construct the cacheUID = compCacheUID@TestID 
	 * @param specificTestResult The SpecificTestResult object were the result of the Test will be store once the Test is done 
	 * @return A CachedSpecificTestResult which should be kept and used to call the {@link CachedSpecificTestResult#commit()}
	 *			when the Test is done. Null is returned if the given Test cannot be cached
	 */
	CachedSpecificTestResult initializeResult(final String compCacheUID, final SpecificTestResult specificTestResult) {
		if( specificTestResult.getTestValidity() == 0 ) {	// skip the tests that cannot be cached
			return null;
		}
		
		final CachedSpecificTestResult cachedEntry = new CachedSpecificTestResult(specificTestResult);
		final String testCacheUID = compCacheUID + "@" + specificTestResult.getTestID();
		
		this.cachedResults.putIfAbsent(testCacheUID, cachedEntry);
		
		return cachedEntry;
		
	}
	
	
	
	/**
	 * Checks for cached versions of the given Test for the given component
	 * 
	 * @param compCacheUID UID of the component to which the Test belongs to. Note that this is not the UID of the comp! @see {@link Tester#getCompCacheUID()}
	 * @param testID ID of the Test for which the cache will be checked for
	 * @return the cached result for the given Test of the given component or null if the result is not cached or is invalid
	 */
	SpecificTestResult getCachedResult(final String compCacheUID, final String testID) {
		final String testKey = compCacheUID + "@" + testID;
		
		final CachedSpecificTestResult cachedEntry = this.cachedResults.get(testKey);
		
		
			if(cachedEntry != null ) {
				while(cachedEntry.isOngoing)
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						Thread.currentThread().interrupt();
					}
				if( cachedEntry.validTill >= System.currentTimeMillis() ) {
					return cachedEntry.specificTestResult;
				} else {
					this.cachedResults.remove(testKey);
					return null;
				}
			} else {
				return null;
			}
		
	}
	

	
	/**
	 * Removes the cache for the given Test of the given component
	 * 
	 * @param compCacheUID CacheUID of the component to which the Test belongs to. Note that this is not the UID of the comp! @see {@link Tester#getCompCacheUID()}
	 * @param testID ID of the Test for which the cache will be checked for
	 * @return The previous value associated with the given Test and Component, or null if there was no such entry in the cache
	 */
	CachedSpecificTestResult reset(final String compCacheUID, final String testID) {
		return this.cachedResults.remove(compCacheUID + "@" + testID);
	}

}
