package jTestManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.atomic.AtomicInteger;

import TM.Executable;
import TM.TestFailure;
import TM.Test;
import config.ConfigObject;
import config.NotFoundException;
import config.attribute_t;
import dal.BaseApplication;
import dal.BaseApplication_Helper;
import dal.Computer;
import dal.Computer_Helper;
import dal.TestableObject;
import dal.TestableObject_Helper;
import ers.Issue;


/**
 * Keeps the information about the ongoing testing of a component( app or generally TestableObject), and launches the first group of
 * independent Tests.
 */
class Tester {

    final private Tm tm;
    final private List<List<Test>> allTestsGroupedInOrder;
    final private AtomicInteger groupsToFinishNum;
    final private AtomicInteger currentlyIndependentTestsToFinish;

    final private BaseApplication baseApp;
    final private TestableObject testableObj;
    final private boolean isApp;
    final private String compID;
    final private ConfigObject compConfigObject;
    final private String baseclassName;
    final private String compCacheUID;

    final private Map<String, Test> tests4Comp;
    final private CompResult compResult;
    final private jTestManager.Callback componentTestDoneCallback;
    final private TmHandle tmHandle;

    final private boolean testerHasSingleTest;

    /**
     * Used only for executing a single test for a specific component
     * 
     * @param app
     * @param test
     * @param callback
     * @param tm
     */
    Tester(final BaseApplication app, final Test test, final jTestManager.Callback callback, final Tm tm) {
        this.tm = tm;
        this.baseApp = app;
        this.testableObj = null;
        this.isApp = true;
        this.compID = app.UID();
        this.compConfigObject = app.config_object();
        this.baseclassName = app.class_name();
        this.compCacheUID = this.baseclassName + "@" + this.compID;

        this.componentTestDoneCallback = callback;
        this.compResult = new CompResult(app.UID());

        this.testerHasSingleTest = true;
        this.tests4Comp = null;

        /*
         * No dependencies to resolve since this is only for single tests
         */

        List<Test> testsList = new ArrayList<>(2);
        testsList.add(test);

        List<List<Test>> list = new ArrayList<>(1);
        list.add(testsList);

        this.allTestsGroupedInOrder = list;

        this.currentlyIndependentTestsToFinish = new AtomicInteger(1); // just 1 Test

        this.groupsToFinishNum = new AtomicInteger(1); // just 1 group
        this.tmHandle = new TmHandle(this.compID);
    }

    /**
     * @param app
     * @param tests4Comp
     * @param callback
     * @param tm
     * @throws WrongConfigurationException
     */
    Tester(final BaseApplication app, final Map<String, Test> tests4Comp, final jTestManager.Callback callback, final Tm tm)
        throws 	config.GenericException, config.NotFoundException, config.SystemException, config.NotValidException, WrongConfigurationException
    {
        this.tm = tm;
        this.baseApp = app;
        this.testableObj = null;
        this.isApp = true;
        this.compID = app.UID();
        this.compConfigObject = app.config_object();
        this.baseclassName = app.class_name();
        this.compCacheUID = this.baseclassName + "@" + this.compID;

        this.componentTestDoneCallback = callback;
        this.compResult = new CompResult(app.UID());

        this.testerHasSingleTest = false;
        this.tests4Comp = tests4Comp;

        /*
         * Resolve dependencies
         */
        DependenciesSolver testDependSolver = null;

        testDependSolver = new DependenciesSolver(tests4Comp);

        this.allTestsGroupedInOrder = testDependSolver.getGroupedTopologicalOrderedTests();

        if(this.allTestsGroupedInOrder.isEmpty()) { // if there are no tests for this app
            this.currentlyIndependentTestsToFinish = null; // set it to null, will not be used anyway in this case
        } else {
            this.currentlyIndependentTestsToFinish = new AtomicInteger(this.allTestsGroupedInOrder.get(0).size());
        }

        this.groupsToFinishNum = new AtomicInteger(this.allTestsGroupedInOrder.size());
        this.tmHandle = new TmHandle(this.compID);
    }

    /**
     * Used only for executing a single test for a specific component
     * 
     * @param testableObj
     * @param test
     * @param callback
     * @param tm
     */
    Tester(final TestableObject testableObj, final Test test, final jTestManager.Callback callback, final Tm tm) {
        this.tm = tm;
        this.baseApp = null;
        this.testableObj = testableObj;
        this.isApp = false; // This is not very nice because it could be an app, since applications are also TestableObjects(?)
        this.compID = testableObj.UID();
        this.compConfigObject = testableObj.config_object();
        this.baseclassName = testableObj.class_name();
        this.compCacheUID = this.baseclassName + "@" + this.compID;

        this.componentTestDoneCallback = callback;
        this.compResult = new CompResult(testableObj.UID());

        this.testerHasSingleTest = true;
        this.tests4Comp = null;

        /*
         * No dependencies to resolve since this is only for single tests
         */

        List<Test> testsList = new ArrayList<>(2);
        testsList.add(test);

        List<List<Test>> list = new ArrayList<>(1);
        list.add(testsList);

        this.allTestsGroupedInOrder = list;

        this.currentlyIndependentTestsToFinish = new AtomicInteger(1); // just 1 Test

        this.groupsToFinishNum = new AtomicInteger(1); // just 1 group
        this.tmHandle = new TmHandle(this.compID);
    }

    /**
     * @param testableObj
     * @param tests4Comp
     * @param callback
     * @param tm
     * @throws WrongConfigurationException
     */
    Tester(final TestableObject testableObj, final Map<String, Test> tests4Comp, final jTestManager.Callback callback, final Tm tm)
        throws 	config.GenericException, config.NotFoundException, config.SystemException, config.NotValidException, WrongConfigurationException
    {
        this.tm = tm;
        this.baseApp = null;
        this.testableObj = testableObj;
        this.isApp = false; // This is not very nice because it could be an app, since applications are also TestableObjects(?)
        this.compID = testableObj.UID();
        this.compConfigObject = testableObj.config_object();
        this.baseclassName = testableObj.class_name();
        this.compCacheUID = this.baseclassName + "@" + this.compID;

        this.componentTestDoneCallback = callback;
        this.compResult = new CompResult(testableObj.UID());

        this.testerHasSingleTest = false;
        this.tests4Comp = tests4Comp;
        /*
         * Resolve dependencies
         */
        DependenciesSolver testDependSolver = null;

        testDependSolver = new DependenciesSolver(tests4Comp);

        this.allTestsGroupedInOrder = testDependSolver.getGroupedTopologicalOrderedTests();

        if(this.allTestsGroupedInOrder.isEmpty()) { // if there are not test for this TestableObject
            this.currentlyIndependentTestsToFinish = null; // will not be used anyway in this case
        } else {
            this.currentlyIndependentTestsToFinish = new AtomicInteger(this.allTestsGroupedInOrder.get(0).size());
        }

        this.groupsToFinishNum = new AtomicInteger(this.allTestsGroupedInOrder.size());
        this.tmHandle = new TmHandle(this.compID);
    }

    /**
     * The actual method that starts the testing for this component
     * 
     * @return handle or null if
     */
    TmHandle test() {

        // System.out.println("Start test of comp: " + this.compID);

        if(this.allTestsGroupedInOrder.isEmpty()) { // if there are not test for this component
            this.compResult.setComponentResultToUnsupported();
            callUsersCallback();
            return this.tmHandle;
        }

        boolean launchedSomeTest = false;

        /*
         * Get the first group and try to launch all it's Tests. If no Tests were launched from the 1st group (because there were valid
         * caches for all of them), then move to the next group
         */
        for(final List<Test> independentTestsList : this.allTestsGroupedInOrder) {
            for(final Test testToRun : independentTestsList) {

                if(Thread.currentThread().isInterrupted()) {
                    ers.Logger.debug(0, "Thread {" + Thread.currentThread().getName() + "} (Tester.run) was interrupted");
                    return null;
                }

                // check the cache
                SpecificTestResult cachedResult = this.tm.getCachedResult(this.compCacheUID, testToRun.UID());
                if(cachedResult != null) {
                    SpecificTestRunnable.cachedTests.incrementAndGet();
                    ers.Logger.debug(1, "Tester: there was a cached version for: " + this.getCompCacheUID() + "@" + testToRun.UID());
                    cachedResult.updateStdout("[This test result was a cached result]");
                    this.compResult.addResult(cachedResult);
                    this.currentlyIndependentTestsToFinish.decrementAndGet();
                } else {
                    // System.out.println("Tester: no cached version found for: " + this.getCompCacheUID() + "@" + testToRun.UID());
                    try {
                        this.tm.getThreadPool().execute(new SpecificTestRunnable(testToRun, this, this.testerHasSingleTest));
                        launchedSomeTest = true;
                    }
                    catch(RejectedExecutionException | config.ConfigException ex) {
                        ers.Logger.error(new ers.Issue(ex));
                        return null;
                    }
                }
            } // endof: for( all independent Tests in group )

            if(launchedSomeTest) {
                break;
            }

            /*
             * If we reach here it means that there were cached results for the whole first group. Adjust the counters and move (loop) too
             * the next group (or finish, if that was the last group)
             */
            if(this.groupsToFinishNum.decrementAndGet() == 0) {
                callUsersCallback();
                return this.tmHandle;
            }
            
            final int nextGroupIndex = this.allTestsGroupedInOrder.size() - this.groupsToFinishNum.get(); // nextGroupIndex ranges : [
                                                                                                          // 1, totalNumOfGroups-1 ]
            final List<Test> nextIndependentTestsSet = this.allTestsGroupedInOrder.get(nextGroupIndex);
            this.currentlyIndependentTestsToFinish.set(nextIndependentTestsSet.size());
        } // endof: for( all groups )

        /*
         * If we reached here, but no Tests were launched (all of them had valid cached results), then the user TM callback needs to be
         * called here
         */
        if(launchedSomeTest == false) {
            callUsersCallback();
        }

        return this.tmHandle;
    }

    /**
     * Given the return value of some Test's executable, this method adds the appropriate Actions to the component's result.
     * 
     * @param test the Test that executed an Executable that finished with some return value
     * @param retValue the return value of the Test's executable
     * @throws InterruptedException
     */
    void addFailures(final Test test, final int retValue)
        throws 	config.ConfigException, InterruptedException {

        for(final TestFailure failure : test.get_Failures()) {

            if(Thread.currentThread().isInterrupted()) {
                throw new InterruptedException("Thread {" + Thread.currentThread().getName() + "} (Tester.addFailures) was interrupted");
            }

            if(failure.get_ReturnCode() != retValue) { // if this Failure does not match our return value, skip it
                continue;
            }

            this.compResult.getActions().add(new Action(failure.get_diagnosis()));

        } // endof for every failure in this Test
    }

    Tm getTm() {
        return this.tm;
    }

    boolean isApp() {
        return this.isApp;
    }

    String getCompID() {
        return this.compID;
    }

    String getCompCacheUID() {
        return this.compCacheUID;
    }

    ConfigObject getCompConfigObject() {
        return this.compConfigObject;
    }

    String getBaseclassName() {
        return this.baseclassName;
    }

    List<List<Test>> getAllTestsGroupedInOrder() {
        return this.allTestsGroupedInOrder;
    }

    CompResult getCompResult() {
        return this.compResult;
    }

    BaseApplication getBaseApp() {
        return this.baseApp;
    }

    TestableObject getTestableObject() {
        return this.testableObj;
    }

    /**
     * Wraps the user callback and calls it in a new thread from the pool
     */
    void callUsersCallback() {
        try {
            this.getTm().getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    Tester.this.componentTestDoneCallback.componentTestDone(Tester.this.compResult);
                }
            });
        }
        catch(RejectedExecutionException ex) {
            ers.Logger.error(new ers.Issue(ex));
        }
    }

    AtomicInteger getCurrentlyIndependentTestsToFinish() {
        return this.currentlyIndependentTestsToFinish;
    }

    AtomicInteger getGroupsToFinishNum() {
        return this.groupsToFinishNum;
    }

    TmHandle getTmHandle() {
        return this.tmHandle;
    }

    Map<String, Test> getTests4Comp() {
        return this.tests4Comp;
    }

    /**
     * Substitutes all the "#this.various.things" references with the appropriate values. The #this keyword refers to the app, the object
     * being tested
     * 
     * @param paramInput A string possibly containing #this references
     * @param confObj The object being tested
     * @return The paramInput String with all its references substituted
     * @throws WrongConfigurationException
     * @throws InterruptedException
     */
    String substituteParam(final String paramInput)
	throws config.ConfigException, WrongConfigurationException, InterruptedException {
        if(paramInput.isEmpty()) {
            return paramInput; // empty, so do nothing
        }
        StringBuilder finalParam = new StringBuilder(32);
        final String param = paramInput.replaceAll("\\s+", " "); // remove multiple whitespaces

        int currentPos = 0;
        int startOfRef;
        int endOfRef;

        while(currentPos < param.length()) {

            if(Thread.currentThread().isInterrupted()) {
                throw new InterruptedException("Thread {" + Thread.currentThread().getName()
                                               + "} (Tester.substituteParam) was interrupted");
            }

            startOfRef = param.indexOf("#this.", currentPos);
            if(startOfRef == -1) { // If #this. not found. Append the rest of the String and stop
                finalParam.append(param.substring(currentPos));
                break;
            }

            // found a ref = (#this.somethings...) at index startOfRef

            finalParam.append(param.substring(currentPos, startOfRef)); // Append everything before the found ref

            final String ref;
            endOfRef = param.indexOf(" ", startOfRef); // Check for the end of the the ref

            if(endOfRef != -1) { // If this ref is not at the end of paramInput
                ref = param.substring(startOfRef, endOfRef); // get the "#this.__stuff_till_whitespace___"
                currentPos = endOfRef; // set current reading position on the whitespace
            } else { // else this ref is at the end of paramInput
                ref = param.substring(startOfRef);
                currentPos = param.length();
            }

            finalParam.append(dereference(ref, this.compConfigObject, false));
        } // while

        ers.Logger.debug(1,
                         "Parameter substitution before: \'" + paramInput + "\' after: \'" + finalParam.toString()
                            + "\' Notice that this is only the result of substituteParam. Further changes may take place after that.");
        return finalParam.toString();
    }

    /**
     * Gets a String starting with "#this." and returns its corresponding substitution. This function assumes correct input.
     * 
     * @param refInput The reference String starting with "#this."
     * @param confObj The object being tested
     * @param forceObj If <i>true</i> this method assumes that any relationship is related to <i>confObj</i> 
     * @return A string corresponding to the appropriate substitution of the reference refInput
     * @throws WrongConfigurationException
     * @throws InterruptedException
     */
    private String dereference(final String refInput, final ConfigObject confObj, final boolean forceObj) 
        throws 	config.SystemException, config.NotFoundException, config.NotValidException, config.GenericException, WrongConfigurationException, InterruptedException { 

        final String ref = refInput.substring(6); // skip "#this." that has size 6

        final String[] tokens = ref.split("\\."); // get tokens by splitting the String around '.'
        final int tokensNum = tokens.length;

        // A token can be:
        //
        // - "UID" keyword : Then value is substituted with the DB unique id of the object
        // - "ClassName" keyword : Then value is substituted with the DB Class Name of the object
        // - attribute of the current object : Then value is substituted with the value of that attribute
        // - relationship to other object : Then object is replaced and cycle continues
        // - reverse relationship, starting with "&" : Then object is replaced and cycle continues

        ConfigObject curObject = confObj;
        String resolvedStr = null;

        for(int i = 0; i < tokensNum; i++) {

            if(Thread.currentThread().isInterrupted()) {
                throw new InterruptedException("Thread {" + Thread.currentThread().getName() + "} (Tester.dereference) was interrupted");
            }

            final String token = tokens[i];

            if(i == tokensNum - 1) { // if this is the last token
                if(token.equals("UID")) {
                    if(i == 0 && this.isApp && forceObj == false) {
                        resolvedStr = this.compID;
                    } else {
                        resolvedStr = curObject.UID();
                    }
                } else if(token.equals("ClassName")) {
                    resolvedStr = curObject.class_name();
                } else { // else, it is an attribute of the current object

                    attribute_t[] attributes = this.tm.getConfig().get_class_info(curObject.class_name(), false).get_attributes();

                    for(attribute_t attribute : attributes) {
                        if(token.equals(attribute.get_name())) {
                            try {
                                switch(attribute.get_type()) {

                                    case string_type:
                                        resolvedStr = curObject.get_string(token);
                                        break;
                                    case bool_type:
                                        resolvedStr = String.valueOf(curObject.get_bool(token));
                                        break;
                                    case double_type:
                                        resolvedStr = String.valueOf(curObject.get_double(token));
                                        break;
                                    case enum_type:
                                        resolvedStr = curObject.get_string(token);
                                        break;
                                    case float_type:
                                        resolvedStr = String.valueOf(curObject.get_float(token));
                                        break;
                                    case s16_type:
                                        resolvedStr = String.valueOf(curObject.get_short(token)); // check
                                        break;
                                    case s32_type:
                                        resolvedStr = String.valueOf(curObject.get_int(token));
                                        break;
                                    case s64_type:
                                        resolvedStr = String.valueOf(curObject.get_long(token));
                                        break;
                                    case s8_type:
                                        resolvedStr = String.valueOf(curObject.get_byte(token)); // check
                                        break;
                                    case u16_type:
                                        resolvedStr = String.valueOf(curObject.get_int(token)); // check Long .toString
                                        break;
                                    case u32_type:
                                        resolvedStr = String.valueOf(curObject.get_int(token)); // check
                                        break;
                                    case u64_type:
                                        resolvedStr = String.valueOf(curObject.get_long(token)); // check
                                        break;
                                    case u8_type:
                                        resolvedStr = String.valueOf(curObject.get_short(token)); // check
                                        break;
                                    case time_type:
                                    case class_type:
                                    case date_type:
                                    default:
                                        // TODO ...
                                        break;
                                } // switch

                                break;

                            }
                            catch(NotFoundException ex) {
                                ers.Logger.error(new Issue("Token: " + token + " resolved to " + attribute.get_type().toString()
                                                           + " but that was probably wrong", ex));
                            }
                        } // if token.equals(attribute.get_name())
                    } // for all attributes

                    if(resolvedStr == null) { // it was the last token, but there was not final resolve
                        throw new WrongConfigurationException("Token \'" + token + "\' of \'" + refInput
                                                              + "\' doesn't seems to be a valid attribute.");
                    }

                }
                return resolvedStr;
            }

            // if here it has to be a relationship to some other object (cannot be an attribute, because it is not the last token)

            if(token.equals("RunsOn") && BaseApplication_Helper.get(this.tm.getConfig(), curObject) != null) {

                if(i == 0 && this.isApp == true && forceObj == false) { // if it is the token right after #this AND #this is an app
                    curObject = this.baseApp.get_host().config_object();
                } else {
                    curObject = curObject.get_object(token);
                }

                if(curObject == null) {
                    throw new WrongConfigurationException("Exception while dereferencing token {" + token
                                                          + "} of the parameters for a testing the {" + this.compID + "} which were {"
                                                          + refInput + "}. The derefernced object from the token was null");
                }

            } else if(token.equals("BackupHosts") && BaseApplication_Helper.get(this.tm.getConfig(), curObject) != null && this.isApp && forceObj == false) { 
                final Computer[] bh = this.baseApp.get_backup_hosts();
                
                // String is something line #this.BackupHosts.UID
                final List<String> tok_list = new ArrayList<>(Arrays.asList(refInput.split("\\.")));                
                tok_list.remove(token); // Remove the token
                
                final String new_ref = String.join(".", tok_list); // String is now #this.UID
                                
                final StringBuilder resolvedRef = new StringBuilder();
                for(final Computer c : bh) {
                    resolvedRef.append(this.dereference(new_ref, c.config_object(), true));
                    resolvedRef.append(' ');
                }
                
                return resolvedRef.toString().trim();                
            } else if(token.startsWith("&")) {
                // If reverse relationship,
                // e.g. #this.&Computer@Interfaces.UID finds the unique ID of the Computer that refers to #this via its relation Interfaces
                // the token would be something like &Computer@Interfaces
                int atSymbolindex = token.indexOf("@");
                String refererClass = token.substring(1, atSymbolindex);
                String relationship = token.substring(atSymbolindex + 1);

                List<ConfigObject> referers = new ArrayList<ConfigObject>();
                curObject.referenced_by(referers, relationship, true); // in C++ it uses the default value which is true

                boolean wasFound = false;
                for(ConfigObject configObj : referers) {
                    if(this.tm.getConfig().try_cast(configObj.class_name(), refererClass)) {
                        curObject = configObj;
                        wasFound = true;
                        break;
                    }
                }

                if(wasFound == false) {
                    throw new WrongConfigurationException("Token \'" + token + "\' of \'" + refInput
                                                          + "\' could not be resolved. (Note: Only composite relationships are checked)");
                }

            } else { // else it is a normal relationship
                try {
                    curObject = curObject.get_object(token); // TODO in cases of multi-values relationship throw something
                }
                catch(Throwable t) {
                    System.err.println("here!");
                }
            }

        } // for every token

        return resolvedStr;
    }

}
