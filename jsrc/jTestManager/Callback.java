package jTestManager;

/**
 * Interface that must be implemented by user's callback class.
 * <p>
 * When a callback is received, the method componentTestDone of this interface is executed
 */
public interface Callback {
	
	/**
	 * The TM user method which will be called when the requested test is done
	 * 
	 * @param compResult The result of the finished testing of the component
	 */
	void componentTestDone(CompResult compResult);
}
