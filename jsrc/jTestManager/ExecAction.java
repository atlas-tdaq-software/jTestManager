package jTestManager;

import java.util.List;

/**
 * 
 * The Action corresponding to the failure ExecUpoFailure.
 *
 */
public class ExecAction extends Action {
	
	private List<Exec> execs;

	public ExecAction(List<Exec> execs, String diagnosis) {
		super(diagnosis);
		this.execs = execs;
	}

	/**
	 * @return the List with all the Exec representing the Executable of this ExecAction
	 */
	public List<Exec> getExecs() {
		return this.execs;
	}
}
